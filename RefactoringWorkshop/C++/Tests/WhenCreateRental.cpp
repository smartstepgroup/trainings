#include "gtest\gtest.h"
#include "BaseTest.h"

class WhenCreateRental : public BaseTest {};

TEST_F(WhenCreateRental, CanGetDaysRented) {
    Rental rental = Create.NewRental(1);

    ASSERT_EQ(1, rental.DaysRented);
}

TEST_F(WhenCreateRental, CanGetMovieRented) {
    Movie& movie = Create.NewMovie();
    Rental rental = Create.NewRental(movie);

    ASSERT_EQ(&movie, &rental.RentedMovie);
}
