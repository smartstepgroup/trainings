#include "gtest\gtest.h"
#include "gmock\gmock.h"
#include "Customer.h"
#include "BaseTest.h"

using namespace testing;

class CanGenerateStatement : public BaseTest {};

TEST_F(CanGenerateStatement, WithHeaderAndFooterIfNoMovies) {
	Customer customer = Create.NewCustomer("Luke Skywalker");

	ASSERT_EQ(string() + 
		"Rental Record for Luke Skywalker\n" +
		"Amount owed is 0.0\n" +
		"You earned 0 frequent renter points", customer.Statement());
}

TEST_F(CanGenerateStatement, ForOneNewMovieOneDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(1, "Star Wars III", Movie::NEW_RELEASE);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars III\t3.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 3.0\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneNewMovieTwoDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(2, "Star Wars Episode I", Movie::NEW_RELEASE);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars Episode I\t6.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 6.0\nYou earned 2 frequent renter points"));
}

TEST_F(CanGenerateStatement, ForOneNewMovieThreeDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(3, "Star Wars VI", Movie::NEW_RELEASE);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars VI\t9.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 9.0\nYou earned 2 frequent renter points"));
}



TEST_F(CanGenerateStatement, ForOneRegularMovieOneDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(1, "Star Wars II", Movie::REGULAR);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t2.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 2.0\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneRegularMovieTwoDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(2, "Star Wars II", Movie::REGULAR);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t2.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 2.0\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneRegularMovieThreeDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(3, "Star Wars II", Movie::REGULAR);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t3.5\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 3.5\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneChildMovieOneDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(1, "Star Wars for Kids", Movie::CHILDRENS);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars for Kids\t1.5\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 1.5\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneChildMovieThreeDay) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(3, "Star Wars for Kids", Movie::CHILDRENS);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars for Kids\t1.5\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 1.5\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForOneChildMovieFourDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(4, "Star Wars for Kids", Movie::CHILDRENS);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars for Kids\t3.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 3.0\nYou earned 1 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForTwoNewMoviesThreeDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(3, "Star Wars I", Movie::NEW_RELEASE)
		.RentMovie(3, "Star Wars II", Movie::NEW_RELEASE);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars I\t9.0\n"));
	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t9.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 18.0\nYou earned 4 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForTwoRegularMoviesThreeDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(3, "Star Wars I", Movie::REGULAR)
		.RentMovie(3, "Star Wars II", Movie::REGULAR);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars I\t3.5\n"));
	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t3.5\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 7.0\nYou earned 2 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForTwoChildMoviesFourDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(4, "Star Wars I", Movie::CHILDRENS)
		.RentMovie(4, "Star Wars II", Movie::CHILDRENS);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars I\t3.0\n"));
	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t3.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 6.0\nYou earned 2 frequent renter points"));
}


TEST_F(CanGenerateStatement, ForAllThreeTypesMoviesFourDays) {
	Customer customer = Create
		.NewCustomer()
		.RentMovie(4, "Star Wars I", Movie::NEW_RELEASE)
		.RentMovie(4, "Star Wars II", Movie::REGULAR)
		.RentMovie(4, "Star Wars III", Movie::CHILDRENS);

	string statement = customer.Statement();

	ASSERT_THAT(statement, HasSubstr("\tStar Wars I\t12.0\n"));
	ASSERT_THAT(statement, HasSubstr("\tStar Wars II\t5.0\n"));
	ASSERT_THAT(statement, HasSubstr("\tStar Wars III\t3.0\n"));
	ASSERT_THAT(statement, EndsWith("Amount owed is 20.0\nYou earned 4 frequent renter points"));
}
