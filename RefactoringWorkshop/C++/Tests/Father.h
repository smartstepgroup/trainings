#pragma once

#include "Movie.h"
#include "Rental.h"
#include "Customer.h"

class CustomerBuilder;

class Father
{
public:
	Father(void);
	~Father(void);

	Movie& NewMovie(const int priceCode);
	Movie& NewMovie(const string title = "", const int priceCode = Movie::REGULAR);
	Rental NewRental(const int daysRented = 0);
	Rental NewRental(const Movie& rentedMovie, int daysRented = 1);
	CustomerBuilder NewCustomer(const string name = "");

private:
	vector<auto_ptr<Movie>> createdMovies;
};

class CustomerBuilder {
private:
	Father& create;
	Customer customer;

public: 
	CustomerBuilder(string name, Father& create) : create(create), customer(name) { }

	CustomerBuilder& RentMovie(int daysRented, string title, int priceCode) {
		Movie& movie = create.NewMovie(title, priceCode);
		Rental rental = create.NewRental(movie, daysRented);
		customer.AddRental(rental);
		return *this;
	}

	operator Customer() { return customer; };
};

