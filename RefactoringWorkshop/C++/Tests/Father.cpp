#include "Father.h"

Movie& Father::NewMovie(const int priceCode) {
    return NewMovie("", priceCode);
}

Movie& Father::NewMovie(const string title, const int priceCode) {
	Movie* newMovie = new Movie(title, priceCode);
	createdMovies.push_back(auto_ptr<Movie>(newMovie));
    return *newMovie;
}

Rental Father::NewRental(const int daysRented) {
    return Rental(daysRented, NewMovie());
}

Rental Father::NewRental(const Movie& rentedMovie, int daysRented) {
    return Rental(daysRented, rentedMovie);
}

CustomerBuilder Father::NewCustomer(const string name) {
	return CustomerBuilder(name, *this);
}


Father::Father(void) {}


Father::~Father(void) {}
