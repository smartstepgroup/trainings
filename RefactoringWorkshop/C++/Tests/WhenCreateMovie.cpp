#include "gtest\gtest.h"
#include "Movie.h"
#include "BaseTest.h"

class WhenCreateMovie : public BaseTest {};

TEST_F(WhenCreateMovie, CanGetPriceCode) {
    Movie& movie = Create.NewMovie(Movie::REGULAR);

    ASSERT_EQ(Movie::REGULAR, movie.PriceCode);
}

TEST_F(WhenCreateMovie, CanGetTitle) {
    Movie& movie = Create.NewMovie("Star Wars");

    ASSERT_EQ("Star Wars", movie.Title);
}

TEST_F(WhenCreateMovie, CanGetTitleAndPriceCode) {
    Movie& movie = Create.NewMovie("Star Wars IV", Movie::NEW_RELEASE);

    ASSERT_EQ("Star Wars IV", movie.Title);
    ASSERT_EQ(Movie::NEW_RELEASE, movie.PriceCode);
}