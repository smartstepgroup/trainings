#include "gtest\gtest.h"
#include "Customer.h"
#include "BaseTest.h"

class WhenCreateCustomer : public BaseTest {};

TEST_F(WhenCreateCustomer, CanGetName) {
	Customer customer = Create.NewCustomer("Dart Weider");

	ASSERT_EQ("Dart Weider", customer.Name);
}