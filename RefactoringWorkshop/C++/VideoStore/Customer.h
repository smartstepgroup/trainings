#pragma once
#include <string>
#include <vector>
#include "Rental.h"

using namespace std;

class Customer
{
public:
	Customer(const string name);
	~Customer(void);

	const string Name;
	void AddRental(Rental& arg);
	string Statement();

private:
	vector<Rental> _rentals;
};

