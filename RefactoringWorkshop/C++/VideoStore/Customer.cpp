#include "Customer.h"

Customer::Customer(const string name)  : Name(name) {}

Customer::~Customer(void) {}

void  Customer::AddRental(Rental& arg) {
	_rentals.push_back(arg);
}



string Customer::Statement() {
	double totalAmount = 0;
	int frequentRenterPoints = 0;

	// Select every inside of the parentheses and extract to method called CreateHeader
	string result = "Rental Record for " + Name + "\n";

	for (int i = 0; i < _rentals.size(); i++) {
        double thisAmount = 0;
		Rental each = _rentals[i];
        switch (each.RentedMovie.PriceCode) 
        {
        case Movie::REGULAR:
            thisAmount += 2;

            if (each.DaysRented > 2) 
            {
                thisAmount += ((each.DaysRented - 2) * 1.5);
            }

            break;

        case Movie::NEW_RELEASE:
            thisAmount += (each.DaysRented * 3);

            break;

        case Movie::CHILDRENS:
            thisAmount += 1.5;

            if (each.DaysRented > 3) 
            {
                thisAmount += ((each.DaysRented - 3) * 1.5);
            }

            break;
        }

		// add frequent renter points
		frequentRenterPoints++;

		// add bonus for a two day new release rental
		if ((each.RentedMovie.PriceCode == Movie::NEW_RELEASE)
			&& (each.DaysRented > 1)) 
		{
			frequentRenterPoints++;
		}

		//show figures for this rental
		// Select every inside of the parentheses and extract to method called CreateLine
		char b1[50];
		sprintf (b1, "%.1f", thisAmount);
		result += ("\t" + each.RentedMovie.Title + "\t" + b1 + "\n");
		totalAmount += thisAmount;
	}

	//add footer lines
	char b2[50];
	sprintf (b2, "%.1f", totalAmount);
	result += "Amount owed is " + string(b2) + "\n";
	result += "You earned " + to_string(frequentRenterPoints) + " frequent renter points";


	return result;
}

