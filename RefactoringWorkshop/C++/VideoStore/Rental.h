#pragma once
#include "Movie.h"

class Rental
{
public:
    Rental(int daysRented, const Movie& movie);
    ~Rental(void);

    const Movie& RentedMovie;
    const int DaysRented;
};

