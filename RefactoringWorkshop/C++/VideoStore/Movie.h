#pragma once

#include <string>

using namespace std;

class Movie {
public:
    Movie(const string title, int priceCode);
    ~Movie(void);

    static const int REGULAR = 0;
    static const int NEW_RELEASE = 1;
    static const int CHILDRENS = 2;

    const string Title;
    const int PriceCode;
};

