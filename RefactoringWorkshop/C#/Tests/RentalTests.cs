#region Usings

using NUnit.Framework;
using VideoStore;

#endregion

namespace Tests {
    [TestFixture]
    public class RentalTests : Test {
        [Test]
        public void GetDaysRented() {
            var rental = Create.Rental(daysRented: 1);
            Assert.AreEqual(1, rental.DaysRented);
        }

        [Test]
        public void GetMovie() {
            var movie = Create.Movie();
            var rental = Create.Rental(movie: movie);
            Assert.AreEqual(movie, rental.Movie);
        }
    }
}