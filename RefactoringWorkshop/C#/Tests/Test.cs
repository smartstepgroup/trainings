#region Usings

using System;
using VideoStore;

#endregion

namespace Tests {
    public abstract class Test {
        protected Test() {
            Create = new Father();
        }

        protected Father Create { get; private set; }

        protected class Father {
            public Movie Movie(string title = null, int priceCode = VideoStore.Movie.REGULAR) {
                title = title ?? UniqueString;
                return new Movie(title, priceCode);
            }

            public Rental Rental(Movie movie = null, int daysRented = 1) {
                movie = movie ?? Movie();
                return new Rental(movie, daysRented);
            }

            public CustomerBuilder Customer(string name = null) {
                name = name ?? UniqueString;
                return new CustomerBuilder(name, this);
            }

            private string UniqueString {
                get { return Guid.NewGuid().ToString(); }
            }

            public class CustomerBuilder {
                private readonly Father create;
                private readonly Customer customer;

                public CustomerBuilder(string name, Father create) {
                    this.create = create;
                    customer = new Customer(name);
                }

                public CustomerBuilder RentMovie(int daysRented, string title, int priceCode) {
                    var movie = create.Movie(title: title, priceCode: priceCode);
                    var rental = create.Rental(movie: movie, daysRented: daysRented);
                    customer.AddRental(rental);
                    return this;
                }

                public static implicit operator Customer(CustomerBuilder builder) {
                    return builder.customer;
                }
            }
        }
    }
}