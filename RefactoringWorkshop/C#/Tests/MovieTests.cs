#region Usings

using NUnit.Framework;
using VideoStore;

#endregion

namespace Tests {
    [TestFixture]
    public class MovieTests : Test {
        [Test]
        public void GetPriceCode() {
            var movie = Create.Movie(priceCode: Movie.REGULAR);
            Assert.AreEqual(Movie.REGULAR, movie.PriceCode);
        }

        [Test]
        public void SetPriceCode() {
            var movie = Create.Movie(priceCode: Movie.CHILDRENS);
            Assert.AreEqual(Movie.CHILDRENS, movie.PriceCode);
        }

        [Test]
        public void GetTitle() {
            var movie = Create.Movie(title: "Star Wars");
            Assert.AreEqual("Star Wars", movie.Title);
        }
    }
}