#include <windows.h>
#include <process.h>
#include <string>
#include <map>
#include <vector>
#include "crypto.h"
#include "main.h"
#include "printer.h"
#include "helpers.h"
#include "resource.h"

#pragma comment (lib,"setupapi.lib")
extern CMain *_app;


//void PrinterThread(PVOID pvParam)
DWORD WINAPI PrinterThread(LPVOID lpParameter) {
	CoInitialize(NULL);
	try
	{
		PAYMENT p;
		CONFIG c;
		DEALER_CONFIG dc;
		CASHOUT co;

		string sBuf;
		__int64 iTransaction = 0;
		HANDLE hEv[5];

		DWORD dwEventRet,dwMutexRet;
		hEv[0] = OpenEvent(EVENT_ALL_ACCESS,FALSE,IEEE_PRINTER_EV_START_PRINT);
		hEv[1] = OpenEvent(EVENT_ALL_ACCESS,FALSE,IEEE_PRINTER_EV_START_PRINT_PREV);
		hEv[2] = OpenEvent(EVENT_ALL_ACCESS,FALSE,IEEE_PRINTER_EV_START_CASHOUT);
		hEv[3] = OpenEvent(EVENT_ALL_ACCESS,FALSE,IEEE_PRINTER_EV_START_PRINT_TEST);
		hEv[4] = OpenEvent(EVENT_ALL_ACCESS,FALSE,IEEE_PRINTER_EV_START_PRINT_VISIT);

		if (hEv[0] && hEv[1] && hEv[2] && hEv[3] && hEv[4]) {
			while(true) {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Ожидание событий печати");
				dwEventRet = WaitForMultipleObjects(5,hEv,false,INFINITE);

				dwMutexRet = WaitForSingleObject(_app->m_pPrinter->hMtxPrinter,I3E_PRINTER_WAITFORPRINTER);
				if (dwMutexRet == WAIT_TIMEOUT) {
					_app->__debug(IEEE_DEBUG_PRINTER,0,"Принтер занят слишком долгое время, пропускаем событие печати");
					continue;
				}

				__zeroStruct(&p);
				__zeroStruct(&co);

				_app->m_pCrypto->getConfig(&c,CRYPTO_CONFIG_READ_TERMINAL_ID);
				_app->m_pCrypto->getDealerConfig(&dc);
				iTransaction = 0;
				bool duplicate = false;
				switch(dwEventRet) {
				case WAIT_OBJECT_0+1:
					{
						_app->__debug(IEEE_DEBUG_PRINTER,0,L"Событие повторной печати чека");
						duplicate = true;
						if (_app->m_pCrypto->getPostValue("gibdd_payment_code", sBuf)) {
							iTransaction = _atoi64(sBuf.c_str());
							p.flags |= CRYPTO_PAYMENT_METADATA_IS_PREVIOUS;
						}
					}
				case WAIT_OBJECT_0:
					{
						start_print(iTransaction, &c, &dc, duplicate);
					}
					break;
				case WAIT_OBJECT_0+2:
					{
						_app->__debug(IEEE_DEBUG_PRINTER,0,L"Событие печати чека инкассации");
						
							bool bPrintOldStyle;
							unsigned long int ulSum = 0;
							bool bFiscSet;

							//check whether function worked perfectly (the parameter is found in xml)
							if(_app->m_pCrypto->GetPrintMode(&bPrintOldStyle) && !bPrintOldStyle)
							{
								wchar_t *lpszRequiredStrings = NULL;

								if(_app->m_pCrypto->GetEncashmentIntoStrings(&lpszRequiredStrings, &ulSum, &bFiscSet) && _app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK)
								{
									if(_app->m_pPrinter->cAtol->dwPrinterModel != 0 && bFiscSet)
									{
										_app->m_pPrinter->cAtol->fSumForFiscCashout = static_cast<float>(ulSum);
										_app->m_pPrinter->cAtol->PrintCashoutThroughXSL(lpszRequiredStrings);
									}
									else
									{
										_app->m_pPrinter->PrintCheckThroughXSL(lpszRequiredStrings);
										_app->m_pPrinter->Cut();
									}
								}
								else
								{
									_app->__debug(IEEE_DEBUG_PRINTER,0,L"Инкассация не получена");
									PostMessage(_app->hParentWindow,WM_TIMER,IEEE_RAS_MAIN_TIMER_ID,0);
								}
							}
							else
							if (_app->m_pCrypto->getCurrentCashout(&co))
							{
								_app->m_pPrinter->PrintCashout(&co, &c);
							}
							else
								_app->__debug(IEEE_DEBUG_PRINTER,0,L"Инкассация не получена");
							
							PostMessage(_app->hParentWindow,WM_TIMER,IEEE_RAS_MAIN_TIMER_ID,0);
					}
					break;
				case WAIT_OBJECT_0+3:
					{
						_app->__debug(IEEE_DEBUG_PRINTER,0,L"Событие печати тестового чека");
						_app->m_pPrinter->PrintTest(&c);
					}
					break;
				case WAIT_OBJECT_0+4:
					{
						_app->__debug(IEEE_DEBUG_PRINTER,0,L"Событие печати визита техника");

						bool bPrintOldStyle;

							//check whether function worked perfectly (the parameter is found in xml)
							if(_app->m_pCrypto->GetPrintMode(&bPrintOldStyle) && !bPrintOldStyle)
							{
								if(!print_visit())
									_app->__debug(IEEE_DEBUG_PRINTER,0,L"Визит не получен");
							}
							else
							{
								VISIT v;

								if(_app->m_pCrypto->getCurrentVisit(&v))
								{
									PostMessage(_app->hParentWindow,WM_TIMER,IEEE_RAS_MAIN_TIMER_ID,0);
									_app->m_pPrinter->PrintVisit(&c, &v);
								}
								else
								{
									_app->__debug(IEEE_DEBUG_PRINTER,0,L"Визит не получен");
								}
							}
					}
					break;
				} //switch
				ReleaseMutex(_app->m_pPrinter->hMtxPrinter);
			} //while
		} //if events
		_app->__debug(IEEE_DEBUG_PRINTER,0,"no events opened");
	}
	catch (...)
	{
		_app->__debug(IEEE_DEBUG_PRINTER,0,L"Сбой потока Printer. Перезагрузка!");
		runRestarter();	
	}
	CoUninitialize();

	return 0;
}

#ifdef _COMM_INCLUDE
string CPrinter::get_comm() {
	wstring _comm = this->wsCom;
	string str(_comm.begin(), _comm.end());
	return str;
}

string CPrinter::get_model(void) {
	return this->dwPrinterModel?this->stPrinterModel:this->cAtol->stPrinterModel;
}
#endif

void start_print(__int64 &iTransaction, CONFIG *c, DEALER_CONFIG *dc, bool duplicate){
	PAYMENT p;
	__zeroStruct(&p);
	PAYMENT p2;
	__zeroStruct(&p2);

	_app->__debug(IEEE_DEBUG_PRINTER,0,L"Событие печати чека");
	p.transactionNumber = iTransaction;
	bool ret = _app->m_pCrypto->getCurrentPayment2(&p);
	__printStruct(&p);

	vector<pair<string, string>> vPost;
	_app->m_pCrypto->getPost(vPost);

	/*Уменьшаем значение fixed_int_summ*************/
	float fFixedSumm = _app->m_pCrypto->reduceFixedSummTo(p.toAmount);

	_app->m_pCrypto->saveTmpPost();
	if(!ret)
		_app->m_pCrypto->clearPostData(false);
	if(ret) {
		DWORD dwPrintRet = 0;
		PROVIDER pr;
		__zeroStruct(&pr);
		pr.prv_id = p.toServiceId;
		_app->m_pCrypto->getProvider(&pr);
		if (p.flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE || p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD || pr.bUseChange) {
			SendMessage(_app->hParentWindow,WM_IEEE_PRINTGIBDD_START,(WPARAM)&p,0);
		}

		if (p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD)
		{
			dwPrintRet = _app->m_pPrinter->PrintGIBDDCheck(&p,c,dc);
		}

		//Если успешно, печатаем сводный чек
		if (dwPrintRet != I3E_PRINT_PRINTERERROR)
		{
			bool bPrintOldStyle;

			//check whether function worked perfectly (the parameter is found in xml)
			if(_app->m_pCrypto->GetPrintMode(&bPrintOldStyle) && !bPrintOldStyle)
			{
				wchar_t *lpszRequiredStrings = NULL;
				float fSumForFiscRegistration;
				bool bFiscSet;

				if(_app->m_pCrypto->FormRequirementXMLForPrint(&lpszRequiredStrings, &fSumForFiscRegistration, &bFiscSet, duplicate))
				{
					if(_app->m_pPrinter->cAtol->dwPrinterModel != 0)
					{
						_app->m_pPrinter->cAtol->fSumForFiscRegistration = fSumForFiscRegistration;
						_app->m_pPrinter->cAtol->bFiscSet = bFiscSet;
					}

					if((p.flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE && _app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK) ||
						p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD   && _app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK)
					{
						SendMessage(_app->hParentWindow, WM_IEEE_CHANGE_PRINT_NAME, (WPARAM)L"tel", (LPARAM)L"Сводный чек");
					}

					if(_app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK)
					{						
						if(duplicate)
						{
							int insertAfter = 2; // УКАЗЫВАЕТ ПОСЛЕ КАКОЙ СТРОКИ ВСТАВИТЬ НАДПИСЬ "ДУБЛИКАТ"							
							int lineCount = 0;
							std::wstringstream wss;

							for(size_t i=0;i<wcslen(lpszRequiredStrings);i++)
							{
								if(lpszRequiredStrings[i] == '\n')
								{
									lineCount++;
									if(lineCount == insertAfter)
									{
										wss << L"\n";
										wss << L"---------------=!-ДУБЛИКАТ-!=-------------\n";
										wss << L"\n";
									}
								}								
								wss << lpszRequiredStrings[i];
							}							
							
							_app->m_pPrinter->PrintCheckThroughXSL(wss.str().c_str(), duplicate);
							_app->m_pPrinter->Cut();
							
						}
						else
						{
							_app->m_pPrinter->PrintCheckThroughXSL(lpszRequiredStrings, duplicate);
							_app->m_pPrinter->Cut();
						}					

						if(_app->m_pPrinter->cAtol->dwPrinterModel != 0)
						{							
							_app->m_pPrinter->cAtol->fSumForFiscRegistration = 0.0;
							_app->m_pPrinter->cAtol->bFiscSet = false;
						}

						if(p.toServiceId == CRYPTO_GIBDD_PROVIDER_ID || p.toServiceId == CRYPTO_PAYMENT_DUMMY_PROVIDER)
							_app->m_pCrypto->decPrinterConfig(3);
						else
							_app->m_pCrypto->decPrinterConfig(0);
					}
				}
				else
				{
					_app->__debug(IEEE_DEBUG_PRINTER,0,L"Платеж не получен");
					PostMessage(_app->hParentWindow,WM_TIMER,IEEE_RAS_MAIN_TIMER_ID,0);
					return;
				}
			}
			else
				_app->m_pPrinter->PrintCheck(&p,c,dc, vPost, duplicate);
		}
		_app->m_pCrypto->clearPostData(false);

		if(_app->m_pCrypto->getCurrentPaymentBy(&p2, p.transactionNumber + 1))
		{
			_app->__debug(IEEE_DEBUG_PRINTER,0,L"Печать второго чека");
			_app->m_pPrinter->PrintCheck(&p2,c,dc, vPost, duplicate);
		}

		//Если платеж ССП, расставляем флаги платежа
		//if (p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD) {
		iTransaction = p.transactionNumber;
		__zeroStruct(&p);
		p.transactionNumber = iTransaction;
		if (_app->m_pCrypto->getCurrentPayment2(&p)) {
			__printStruct(&p);
			if (dwPrintRet != I3E_PRINT_PRINTERERROR || !(p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD)) {
				if (p.flags & CRYPTO_PAYMENT_METADATA_PRINTER_ERROR) {
					p.flags ^= CRYPTO_PAYMENT_METADATA_PRINTER_ERROR;
				}
				if (p.flags & CRYPTO_PAYMENT_METADATA_FISK_ERROR) {
					p.flags ^= CRYPTO_PAYMENT_METADATA_FISK_ERROR;
				}
				_app->m_pCrypto->getCurrentPayment2(&p,true);
				__printStruct(&p);
			} else {
				p.flags |= CRYPTO_PAYMENT_METADATA_PRINTER_ERROR;
				p.flags |= CRYPTO_PAYMENT_METADATA_FISK_ERROR;
				_app->m_pCrypto->getCurrentPayment2(&p,true);	
				__printStruct(&p);
			}
		} //if current payment
		//}

		if (p.flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE || p.flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD || pr.bUseChange) {
			SendMessage(_app->hParentWindow,WM_IEEE_PRINT_END,(WPARAM)&p,dwPrintRet);
			SendMessage(_app->hParentWindow, WM_IEEE_CHANGE_PRINT_NAME, (WPARAM)L"tel", (LPARAM)L"Печать чеков завершена!");
		}

		if (pr.bOfferRepeat && fFixedSumm > 0) {
			SendMessage(_app->hParentWindow,WM_IEEE_OFFER_REPEAT,0,0);
		}
		p.flags |= CRYPTO_PAYMENT_METADATA_CHECK_DONE;
		_app->m_pCrypto->getCurrentPayment2(&p,true);	
	}
	else{
		_app->__debug(IEEE_DEBUG_PRINTER,0,L"Платеж не получен");
	}

	//Достали платеж, запускаем отправку
	PostMessage(_app->hParentWindow,WM_TIMER,IEEE_RAS_MAIN_TIMER_ID,0);
	////////////////////////
}

bool print_visit(void)
{
	HANDLE hSeparateHeap;
	wchar_t *lpszVisit = NULL;
	wchar_t **lpszAttributes = NULL, **lpszTemplateVisitCheckLines = NULL, **lpszRealVisitCheckLines = NULL;
	unsigned int nLines = 0;
	wchar_t lpszCount[8];
	wchar_t lpszSize[8];
	PRINTER_CONFIG pc;
	bool bFuncSuccessfulness = false;
	
	_app->m_pCrypto->getPrinterConfig(&pc);
	_itow_s(pc.rollSize / 1000, lpszSize, _countof(lpszSize), 10);

	unsigned int nBills[CCNET_BILL_TYPES];
	ZeroMemory(&nBills, CCNET_BILL_TYPES * sizeof(unsigned int));
	_app->m_pCrypto->getCcnetStat(nBills);
	getBillCount(&nLines);
	_itow_s(nLines, lpszCount, _countof(lpszCount), 10);
	wchar_t lpszModel1[80], lpszModel2[80], lpszModel3[80];

	MultiByteToWideChar(CP_ACP, 0, _app->m_pWatchDog->WatchDogVersionName.c_str(), -1, lpszModel1, 80);
	MultiByteToWideChar(CP_ACP, 0, _app->m_pPrinter->stPrinterModel.c_str(), -1, lpszModel2, 80);
	MultiByteToWideChar(CP_ACP, 0, _app->m_pCashCode->stModelName.c_str(), -1, lpszModel3, 80);

	if(_app->m_pCrypto->PutAppliancesStateInQueue(L"root", L"appliance",	L"name",			L"Сторожевой таймер",
																			L"model",			lpszModel1,
																			L"state",			L"Not defiend yet") &&

	   _app->m_pCrypto->PutAppliancesStateInQueue(L"root", L"appliance",	L"name",			L"Принтер",
																			L"model",			lpszModel2,
																			L"state",			_app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK ?
																								L"Ok" : L"Неисправен",
																			L"tape_remain",		L"0.00 м.",//lpszSize,
																			L"fisk_mode",		1 == 0 ?//lppr->stObjs.pCAppliances->pPrinter->EnterRegTest() ?
																								L"Да." : L"Нет.") &&

		_app->m_pCrypto->PutAppliancesStateInQueue(L"root", L"appliance",	L"name",			L"Купюроприёмник",
																			L"model",			lpszModel3,
																			L"state",			_app->m_pCashCode->dwLastState == I3E_STATUS_OK ?
																								L"Ok" : L"Неисправен",
																								L"bill_amount",		lpszCount))
	if(_app->m_pCrypto->GetTechVisitIntoStrings(&lpszVisit) && _app->m_pPrinter->UpdateStatus() == I3E_STATUS_OK)
	{
		_app->m_pPrinter->PrintCheckThroughXSL(lpszVisit);
		_app->m_pPrinter->Cut();
		HeapFree(GetProcessHeap(), 0, lpszVisit);
		bFuncSuccessfulness = true;
	}
	return bFuncSuccessfulness;
}

CPrinter::CPrinter() : dwPrinterModel(0), dwPrinterStatus(0)
{
	bWinspool = false;
	this->hThread = 0;
	this->nThreadCheckPoint = 0;
	this->cAtol = new CAtol();
}

CPrinter::~CPrinter()
{
}

bool CPrinter::Init()
{
	bool fret = false;
	BYTE bID = 0;
	BYTE bInit[] = {0x1D,0x40};
	BYTE bInitAV268[] = {0x1B,0x76};
	BYTE bGetID[] = {0x1D,0x49,1};
	BYTE bStatusStar[] = {0x04};
	BYTE bGetName[] = {0x1D,0x49,67};
	const unsigned int nModelLength = 256;
	char szModel[nModelLength] = {0};
	DWORD dwReaded = 0;
	string sCom, sBuf;

	for (int i = 0; i < _app->vsCom.size(); i++)
	{
		memset(szModel,0,256);
		wstring wsPort = L"\\\\.\\" + _app->vsCom[i].stComName;
		
		_app->__debug(IEEE_DEBUG_PRINTER,NULL,L"Открытие COM-порта: %s",wsPort.c_str());

		hPort = CreateFile(wsPort.c_str(), GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if (hPort != INVALID_HANDLE_VALUE)
		{
			_app->__debug(IEEE_DEBUG_PRINTER,NULL,L"Порт открыт");
			if (ComPortConfig(hPort,19200,8,0,0,0,50,20,10,10,150)) {
			//if (ComPortConfig(hPort,57600,8,0,0,0,50,20,10,10,150)) {
				Sleep(1000);
				if (WriteComPort(hPort,bInit,sizeof(bInit),0,0,0,0)) {
					Sleep(1000);
					if (WriteComPort(hPort,bGetID,sizeof(bGetID),true,&bID,1,&dwReaded) && dwReaded != 0) {
						_app->__debug(IEEE_DEBUG_PRINTER,0,"ID принтера %d",bID);
						switch (bID) {
							case 0x5E:
							case 0x5D:
								{
									BYTE bTypeID = 0;
									BYTE bGetTypeID[] = {0x1D,0x49,2};
									BYTE bRomID[5] = {0};
									BYTE bRomTypeID[] = {0x1D,0x49,3};
									this->stPrinterModel = "Custom VKP 80";
									this->dwPrinterModel = IEEE_PRINTER_M_CUSTOMVKP80;

									/*if (WriteComPort(hPort,bGetTypeID,sizeof(bGetTypeID),true,&bTypeID,sizeof(bTypeID),&dwReaded) && dwReaded != 0) {
										_app->__debug(IEEE_DEBUG_PRINTER,0,"Custom: type id 0x%0x", bTypeID);
									}*/
									if (WriteComPort(hPort,bRomTypeID,sizeof(bRomTypeID),true,bRomID,sizeof(bRomID),&dwReaded) && dwReaded != 0) {
										this->stPrinterModel.append(" ");
										this->stPrinterModel.append((char *)bRomID);
										this->stPrinterROM = (char *)bRomID;
									}
								}
								break;
							case 0x75:
							case 0x30:
								{
									this->stPrinterModel = "Citizen PPU700";
									this->dwPrinterModel = IEEE_PRINTER_M_CITIZENPPU700;
								}
								break;
							case 0x51:
								{
									this->stPrinterModel = "Citizen S2000";
									this->dwPrinterModel = IEEE_PRINTER_M_CITIZENS2000;
								}
								break;
							case 0x93:
								{
									this->stPrinterModel = "Custom TG2480";
									this->dwPrinterModel = IEEE_PRINTER_M_CUSTOMTG2480;
								}
								break;
							case 0xA8:
								{
									this->stPrinterModel = "Custom TG2480H";
									this->dwPrinterModel = IEEE_PRINTER_M_CUSTOMTG2480;
								}
								break;
						} //switch
					} //if get ID
				} else { //if Init
					_app->__debug(IEEE_DEBUG_PRINTER,0,"Ошибка отправки команды INIT");
				}
			} else { //if port config
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Ошибка конфигурирования порта");
			}

			//Если не нашли, пробуем найти принтер Star
			if (this->dwPrinterModel == 0) {
				if (ComPortConfig(hPort,9600,8,0,0,0,50,20,10,10,150)) {
					if (WriteComPort(hPort,bStatusStar,sizeof(bStatusStar),true,&bID,1,&dwReaded) && dwReaded != 0) {
						_app->__debug(IEEE_DEBUG_PRINTER,0,"Статус возможного принтера Star 0x%X", bID);
						if (bID & 16 && !(bID & 1) && !(bID & 128)) {
						//if (!(bID & 0x0F)) {
							this->dwPrinterModel = IEEE_PRINTER_M_STAR;
							this->stPrinterModel = "Star Micronics";
						}
					} //if get star status
				} else { //if port config
					_app->__debug(IEEE_DEBUG_PRINTER,0,"Ошибка конфигурирования порта");
				}
			} //if model 0

			//Если не нашли, пробуем найти принтер AV268
			if (this->dwPrinterModel == 0) {
				//if (ComPortConfig(hPort,115200,8,0,0,0,100,5,100,5,50)) {
					CloseHandle(hPort);
					hPort = CreateFile(wsPort.c_str(), GENERIC_READ | GENERIC_WRITE,FILE_SHARE_READ, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
					if (hPort != INVALID_HANDLE_VALUE) {
						DWORD dwRet, temp;
			
						PurgeComm(hPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

						COMSTAT comstat;
						ZeroMemory(&comstat, sizeof(COMSTAT));
						ClearCommError(hPort, &temp, &comstat);
						
						SERIAL_BAUD_RATE sb;
						sb.BaudRate = 115200;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_BAUD_RATE, (LPVOID)&sb, sizeof(SERIAL_BAUD_RATE), 0, 0, &dwRet, NULL);
						DeviceIoControl(hPort, IOCTL_SERIAL_CLR_RTS, 0, 0, 0, 0, &dwRet, NULL);
						DeviceIoControl(hPort, IOCTL_SERIAL_CLR_DTR, 0, 0, 0, 0, &dwRet, NULL);

						SERIAL_LINE_CONTROL slc;
						slc.Parity = NO_PARITY;
						slc.StopBits = STOP_BIT_1;
						slc.WordLength = 8;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_LINE_CONTROL, (LPVOID)&slc, sizeof(SERIAL_LINE_CONTROL), 0, 0, &dwRet, NULL);

						SERIAL_CHARS sc;
						sc.EofChar = 0;
						sc.ErrorChar = 0;
						sc.BreakChar = 0;
						sc.EventChar = 0;
						sc.XonChar = 0x11;
						sc.XoffChar = 0x13;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_CHARS, (LPVOID)&sc, sizeof(SERIAL_CHARS), 0, 0, &dwRet, NULL);


						SERIAL_HANDFLOW sh;
						sh.ControlHandShake = 0;
						sh.FlowReplace = 1;
						sh.XoffLimit = 0;
						sh.XonLimit = 0;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_HANDFLOW, (LPVOID)&sh, sizeof(SERIAL_HANDFLOW), 0, 0, &dwRet, NULL);

						SERIAL_TIMEOUTS st;
						st.ReadIntervalTimeout = 50;
						st.ReadTotalTimeoutMultiplier = 5;
						st.ReadTotalTimeoutConstant = 100;
						st.WriteTotalTimeoutMultiplier = 5;
						st.WriteTotalTimeoutConstant = 100;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_TIMEOUTS, (LPVOID)&st, sizeof(SERIAL_TIMEOUTS), 0, 0, &dwRet, NULL);

						SERIAL_QUEUE_SIZE sqs;
						sqs.InSize = 1024;
						sqs.OutSize = 1024;
						DeviceIoControl(hPort, IOCTL_SERIAL_SET_QUEUE_SIZE, (LPVOID)&sqs, sizeof(SERIAL_QUEUE_SIZE), 0, 0, &dwRet, NULL);

						if (WriteComPort(hPort,bInitAV268,sizeof(bInitAV268),true,&bID,1,&dwReaded) && dwReaded == 1) {
							_app->__debug(IEEE_DEBUG_PRINTER,0,"Статус возможного притера av268 0x%X", bID);
							if (!(bID & 16) && !(bID & 128)) {
								this->dwPrinterModel = IEEE_PRINTER_M_AV268;
								this->stPrinterModel = "AV-268";

								BYTE bAV[] = {0x1B, 0x40};
								WriteComPort(hPort,bAV,sizeof(bAV));
							}
						} //if get av268
					} //if hPort
				/*} else { //if port config
					_app->__debug(IEEE_DEBUG_PRINTER,0,"portconfig error");
				}*/
			} //if model 0

			//Если не нашли, пробуем найти ФР АТОЛ
			if (this->dwPrinterModel == 0) {
				if (this->cAtol->Init(hPort)) {
					this->dwPrinterModel = this->cAtol->dwPrinterModel;
					this->stPrinterModel = this->cAtol->stPrinterModel;
				} //if init ok
			} //if model 0

			//Если нашли нужны ID, считаем инициализацию успешной
			if (this->dwPrinterModel != 0 && hPort != INVALID_HANDLE_VALUE) {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Найден принтер: %s",this->stPrinterModel.c_str());
				fret = true;
				unicodeToAnsi_s(_app->vsCom[i].stComName, sCom);
				this->wsCom = _app->vsCom[i].stComName;
				break;
			} //if model

			CloseHandle(hPort);
		} else { //if open
			_app->__debug(IEEE_DEBUG_PRINTER,NULL,L"Ошибка открытия порта");
		}
	}
	if (fret)
	{
		HANDLE hEv[5];
		hEv[0]= CreateEvent(0,FALSE,FALSE,IEEE_PRINTER_EV_START_PRINT);
		hEv[1] = CreateEvent(0,FALSE,FALSE,IEEE_PRINTER_EV_START_CASHOUT);
		hEv[2] = CreateEvent(0,FALSE,FALSE,IEEE_PRINTER_EV_START_PRINT_PREV);
		hEv[3] = CreateEvent(0,FALSE,FALSE,IEEE_PRINTER_EV_START_PRINT_TEST);
		hEv[4] = CreateEvent(0,FALSE,FALSE,IEEE_PRINTER_EV_START_PRINT_VISIT);

		this->hMtxPrinter = CreateMutex(0,false,I3E_PRINTER_MUTEX_OPERATION);
		if (!hEv[0] || !hEv[1] || !hEv[2] || !hEv[3] || !hEv[4] || !this->hMtxPrinter) {
			_app->__debug(IEEE_DEBUG_PRINTER,0,L"error while print events creating");
			fret = false;
		}
		else
		{
			FormatString(sBuf, "Принтер %s, порт %s", this->stPrinterModel.c_str(), sCom.c_str());
			SendMessage(_app->hInitDeviceWindow, WM_IEEE_INIT_CHANGE_VALUE, (WPARAM)IDC_INIT_DEVICE_PRINTER, (LPARAM)sBuf.c_str());
			_app->__debug(IEEE_DEBUG_PRINTER,0,"Принтер успешно инициализирован");
			
			//_beginthread(PrinterThread,0,0);
			DWORD dwThreadId = 0;
			this->hThread = CreateThread(NULL, NULL, PrinterThread, NULL, NULL, &dwThreadId);

			//this->UpdateStatus();
		}
	} else
	{
		SendMessage(_app->hInitDeviceWindow, WM_IEEE_INIT_CHANGE_VALUE, (WPARAM)IDC_INIT_DEVICE_PRINTER, (LPARAM)"Принтер не был найден");
	}
	return fret;
}

bool CPrinter::PrintCheckThroughXSL(const __in wchar_t *lpszCheckCompleteContent, bool duplicate)
{
	int iSize;
	char *lpszCheckContent = NULL;
	bool fret = false;

	_prepare(I3E_CHECK_NORMAL);

	iSize = WideCharToMultiByte(CP_ACP, 0, lpszCheckCompleteContent, -1, NULL, 0, NULL, NULL);

	if(iSize && (lpszCheckContent = static_cast<char *>(HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, iSize + 1))) != NULL)
	{
		WideCharToMultiByte(CP_ACP, 0, lpszCheckCompleteContent, -1, lpszCheckContent, iSize, NULL, NULL);

		if(cAtol->dwPrinterModel == 0)
		{
			string s;

			CharToOemA(lpszCheckContent, lpszCheckContent);
			s = lpszCheckContent;
			_printline(false, s);
			_postprintspace(4);
			fret = true;
		}
		else
		{
			cAtol->PrintCheckThroughXSL(lpszCheckCompleteContent, duplicate);
		}
		HeapFree(GetProcessHeap(), 0, lpszCheckContent);
		return true;
	}

	return false;
}

bool CPrinter::PrintCheck(PAYMENT *p,CONFIG *c,DEALER_CONFIG *dc, vector<pair<string, string>> vPost, bool duplicate) {
	if (UpdateStatus() == I3E_STATUS_OK) {
		//if(bWinspool)
		_update_spooler();

		if (this->cAtol->dwPrinterModel != 0) {
			return this->cAtol->PrintCheck(p, c, dc);
		}

		string sKey, sValue;
		map<string,string>::iterator it;
		int pos = 0;

		this->_prepare(I3E_CHECK_NORMAL);

		if (p->flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE || p->flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD) {
			SendMessage(_app->hParentWindow, WM_IEEE_CHANGE_PRINT_NAME, (WPARAM)L"tel", (LPARAM)L"Сводный чек");
		}
		
		//Реклама сверху//////
		/*it = mPost.find("_receipt_prt_top_reklama");
		if (it != mPost.end()) {
			sValue = it->second;
			while (sValue.find("<br>") != -1) {
				pos = sValue.find("<br>");
				sValue.replace(pos, 4, "\r\n");
			}
			PrintLine(true,"%s",sValue.c_str());
			mPost.erase(it);
		}*/
		/*
		for (int ii = 0; ii < vPost.size(); ii++) {
			if (vPost[ii].first == "_receipt_prt_top_reklama") {
				sValue = vPost[ii].second;
				while (sValue.find("<br>") != -1) {
					pos = sValue.find("<br>");
					sValue.replace(pos, 4, "\r\n");
				}
				PrintLine(true,"%s",sValue.c_str());
			} //if found
		} //for
		*/
		//////////////////////

		PROVIDER pr;
		__zeroStruct(&pr);
		pr.prv_id = p->toServiceId;
		_app->m_pCrypto->getProvider(&pr);

		vector<PAYSYSTEM> ps;
		_app->m_pCrypto->getPaysystems(p, ps);

		if(p->toServiceId == 100078 || p->toServiceId == 100083)
		{
			PrintLine(true,"http://www.contact-sys.com/");
		}
		else
		{
			PrintLine(true,"** Оплачивайте туры в терминалах TourPay *");
			PrintLine(true,"***************** TourPay ****************");
			if(duplicate)
			{
				PrintLine(true,"");
				PrintLine(true,"---------------=!-ДУБЛИКАТ-!=-------------");
			}
		}

		PrintLine(true,"");
		PrintLine(true,"Платежный агент: %s", dc->name.c_str());
		PrintLine(true,"ИНН: %s", dc->inn.c_str());
		PrintLine(true,"Адрес: %s", dc->address.c_str());

		for (int j = 0; j < ps.size(); j++) {
			if (ps[j].name == L"osmp") {
				this->PrintLine(true,"Служба поддержки: %s",dc->phone.c_str());
				break;
			}
			if (ps[j].name == L"ssp") {
				if(p->toServiceId == 100078 || p->toServiceId == 100083)
				{
					this->PrintLine(true,"Оператор по приему платежей:");
					this->PrintLine(true,"АКБ «РУССЛАВБАНК» (ЗАО)");
					this->PrintLine(true,"Москва, ул.Донская,14, стр.2,");
					this->PrintLine(true,"лицензия №1073, ИНН 7706193043");
					this->PrintLine(true,"тел. 8 800 200 42 42");
				}
				else
					if(pr.prv_id == 100145)
				{
					PrintLine(true, "Наименование оператора: ООО НКО");
					PrintLine(true, "\"Рапида\"");
					PrintLine(true, "Адрес (оператора): 125190, Москва, ул.");
					PrintLine(true, "Усиевича, д.20, корп. 2");
					PrintLine(true, "ИНН (оператора) 7744000775");
				}
				else
				{
					this->PrintLine(true,"Платежная система TourPay: (495) 287-48-95");
					this->PrintLine(true, "(800) 700-73-12, (495) 775-77-29");
					this->PrintLine(true, "www.tourpay.ru");
				}
				break;
			}
		} //for paysystems

		this->PrintLine(true,"");
		this->PrintLine(true,"Терминал: %d", c->osmpId);
		this->PrintLine(true,"Адрес терминала: \n%s", dc->t_address.c_str());
		if(pr.prv_id == 100145)
		{
			this->PrintLine(true,"Телефон поддержки: %s",dc->phone.c_str());
		}
		this->PrintLine(true,"");
		this->PrintLine(true,"");

		std::string sName = "", sPhone = "";
		for(int i=0;i<vPost.size();i++)
		{
			if(vPost[i].first == "disp6")
				sName = vPost[i].second;
			else if(vPost[i].first == "disp7")
				sPhone = vPost[i].second;
		}		

		if(sName != "")
			this->PrintLine(true,"Поставщик: %s", sName.c_str());
		else
			this->PrintLine(true,L"Поставщик: %s", pr.wsName.c_str());

		this->PrintLine(true,L"%s", pr.wsSName.c_str());

		if(sPhone != "")
			this->PrintLine(true,"Телефон Поставщика: %s", sPhone.c_str());
		else
			this->PrintLine(true,L"Телефон Поставщика: %s", pr.wsSupPhone.c_str());

		this->PrintLine(true,"Сумма: %.2f",p->fromAmount);
		this->PrintLine(true,"Зачислено: %.2f",p->toAmount);
		this->PrintLine(true,"Комиссия: %.2f",(float)abs((int)((p->fromAmount - p->toAmount - p->change) * 1000)) / 1000);
		if (p->change != 0) {
			this->PrintLine(true,"");
			this->PrintLine(true,"Неиспользованный остаток: %.2f",p->change);
			this->PrintLine(true,"Код для использования: %I64d",p->transactionNumber);
			this->PrintLine(true,"");
		}
		this->PrintLine(true,L"Номер телефона/счета: %s",p->toAccountNumber.c_str());
		this->PrintLine(true,"Номер квитанции: %d",p->receiptNumber);
		this->PrintLine(true,"Номер транзакции: %I64d",p->transactionNumber);
		this->PrintLine(true,"Дата: %.2d.%.2d.%d %.2d:%.2d:%.2d",p->datetime.wDay,p->datetime.wMonth,p->datetime.wYear,p->datetime.wHour,p->datetime.wMinute,p->datetime.wSecond);
		this->PrintLine(true,"");
		this->PrintLine(true,"Сохраняйте чек до зачисления денег\r\nна Ваш лицевой счет. Спасибо!");
		if(pr.prv_id == 100145)
		{
			PrintLine(true, "Телефон ООО НКО \"Рапида\": (495) 3801777");
		}
		this->PrintLine(true,"");
		
		//Костыль для ЛК - печатаем поля _receipt
		string sPattern = "_receipt_";
		
		typedef map<string, string>::const_iterator CI;
		for (int ii = 0; ii < vPost.size(); ii++) {
			if (vPost[ii].first.length() > sPattern.length() && vPost[ii].first.substr(0, sPattern.length()) == sPattern) {
				sKey = vPost[ii].first;
				sValue = vPost[ii].second;

				sKey.erase(0, sPattern.length());
				if (sKey.length() > 4 && sKey.substr(0, 4) != "prt_") {
					sKey += ":";
					while (sValue.find("<br>") != -1) {
						pos = sValue.find("<br>");
						sValue.replace(pos, 4, "\r\n");
					}
					PrintLine(false, "%s", sKey.c_str());
					this->_set_emphasis(1);
					PrintLine(true, "%s", sValue.c_str());
					this->_set_emphasis(0);
				} //if prt
			} //if pattern
		}//for
		PrintLine(true,"");
		//////////////////////////

		for (int j = 0; j < ps.size(); j++) {
			if (ps[j].name == L"osmp") {
				this->PrintLine(true,"Справочная служба QIWI: 8-495-6265252");
				this->PrintLine(true,"**************Круглосуточно***************");
				break;
			}
			if (ps[j].name == L"ssp") {
				this->PrintLine(true,"******************************************");
				if(p->toServiceId == 100078 || p->toServiceId == 100083)
				{
					this->PrintLine(true,"Справки и претензии по тел:");
					this->PrintLine(true," 8-800-200-42-42");
				}
				if(pr.prv_id == 100145)
				{
					PrintLine(true, "Служба поддержки: 8-800-555-444-1");
				}
				else
				{
					this->PrintLine(true,"Служба поддержки: %s",dc->phone.c_str());
				}

				this->PrintLine(true,"******************************************");
				break;
			}
		} //for paysystems

		//Реклама снизу//////
		/*it = mPost.find("_receipt_prt_bottom_reklama");
		if (it != mPost.end()) {
			sValue = it->second;
			while (sValue.find("<br>") != -1) {
				pos = sValue.find("<br>");
				sValue.replace(pos, 4, "\r\n");
			}
			PrintLine(true,"%s",sValue.c_str());
			mPost.erase(it);
		}*/
		if (p->toServiceId != CRYPTO_PAYMENT_DUMMY_PROVIDER) {
			for (int ii = 0; ii < vPost.size(); ii++) {
				if (vPost[ii].first == "_receipt_prt_bottom_reklama") {
					sValue = vPost[ii].second;
					while (sValue.find("<br>") != -1) {
						pos = sValue.find("<br>");
						sValue.replace(pos, 4, "\r\n");
					}
					PrintLine(true,"%s",sValue.c_str());
				} //if found
			} //for

			it = p->mExtra.find("receipt_prt_bottom_reklama");
			if (it != p->mExtra.end()) {
				sValue = it->second;
				while (sValue.find("<br>") != -1) {
					pos = sValue.find("<br>");
					sValue.replace(pos, 4, "\r\n");
				}
				PrintLine(true,"%s",sValue.c_str());
			}
		} //if not dummy
		//////////////////

		/*{
			const char *LineTemplate="ИТОГ =                              ";

			char SumBuffer[0x100];
			char LineBuffer[0x100];
			strcpy(LineBuffer,LineTemplate);
			sprintf(SumBuffer,"%.2f",p->toAmount);
			int SumLen=strlen(SumBuffer);
			int _pos=strlen(LineBuffer)-SumLen-1;
			char *SumStr=LineBuffer+_pos;
			strcpy(SumStr,SumBuffer);

			this->PrintLine(true,"");
			this->PrintLine(true,"");

			this->_set_font(48);
			this->_set_leftmargin(50,0);
			this->PrintLine(true,LineBuffer);
			this->_set_font(0);
			this->_set_leftmargin(10,0);
		}*/

		this->_postprintspace(4);

		Cut();

		if (p->toServiceId == CRYPTO_GIBDD_PROVIDER_ID || p->toServiceId == CRYPTO_PAYMENT_DUMMY_PROVIDER) {
			_app->m_pCrypto->decPrinterConfig(3);
		} else {
			_app->m_pCrypto->decPrinterConfig(0);
		}

		/*if ((p->flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE && !(p->flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD)) || (p->flags & CRYPTO_PAYMENT_METADATA_IS_DUMMY && !(p->flags & CRYPTO_PAYMENT_METADATA_IS_GIBDD))) {
			if (this->UpdateStatus() == I3E_STATUS_OK) {
				this->_sleep();
				SendMessage(_app->hParentWindow, WM_IEEE_CHANGE_PRINT_NAME, (WPARAM)L"tel", (LPARAM)L"Чек на сдачу");
				_printgibddch(p,c);
				this->Cut();
				_app->m_pCrypto->decPrinterConfig(2);
			}
		}*/

		return true;
	}
	else {
		_app->__debug(IEEE_DEBUG_PRINTER,0,"Ошибка печати чека! Состояние принтера %d.", this->dwPrinterStatus);
		return false;
	}
}

DWORD CPrinter::PrintGIBDDCheck(PAYMENT *p,CONFIG *c,DEALER_CONFIG *dc)
{
	DWORD dwRet = 0;
	char *szKvCount = 0;
	wstring wsBuf;
	vector<m_kv> vec_kv;
	if (p->mExtra.find("kol_kv") != p->mExtra.end())
	{
		unsigned int nCount = atoi(p->mExtra["kol_kv"].c_str());
		if (nCount > 0)
		{
			for (int i = 1; i <= nCount; i++)
			{
				m_kv m;
				string summ, type;
				this->_getextra(p,m.kv_name,"kv_name%d",i);
				this->_getextra(p,m.kv_rekv,"kv_rekv%d",i);
				this->_getextra(p,m.kv_oper,"kv_oper%d",i);
				this->_getextra(p,type,"kv_rekvs%d",i);
				this->_getextra(p,summ,"kv_summ%d",i);
				m.kv_summ = atof(summ.c_str());
				m.kv_type = atoi(type.c_str());

				vector<m_kv>::iterator vit = find(vec_kv.begin(),vec_kv.end(),m);
				if (vit != vec_kv.end() && c->nCheckType == 2)
				{
					vit->kv_summ += m.kv_summ;
					vit->kv_name.append("\r\n");
					vit->kv_name.append(m.kv_name);
				}
				else
				{
					vec_kv.insert(vec_kv.begin(),m);
				}
			}

			if ((p->flags & CRYPTO_PAYMENT_METADATA_IS_PREVIOUS && p->flags & CRYPTO_PAYMENT_METADATA_PRINTER_ERROR) || !(p->flags & CRYPTO_PAYMENT_METADATA_IS_PREVIOUS))
			{
				if (p->flags & CRYPTO_PAYMENT_METADATA_IS_DUMMY)
				{
					/*if (this->UpdateStatus() == I3E_STATUS_OK)
					{
						_printgibddch(p,c);
						this->Cut();
						_app->m_pCrypto->decPrinterConfig(2);
						this->_sleep();
					}*/
				}
				else
				{
					if (vec_kv.size() > 0)
					{
						for(vector<m_kv>::iterator i = vec_kv.begin(); i != vec_kv.end(); ++i)
						{
							if (this->UpdateStatus(IEEE_PRINTER_PRINT_SLEEP_EXTRA) == I3E_STATUS_OK) {
								//if(bWinspool)
								_update_spooler();
								ansiToUnicode_s(i->kv_name, wsBuf);
								SendMessage(_app->hParentWindow, WM_IEEE_CHANGE_PRINT_NAME, (WPARAM)L"tel", (LPARAM)wsBuf.c_str());
								
								//Проверяем, не заданы ли копии в конфиге
								int nCopies = _app->m_pCrypto->getCopies(atoi(i->kv_oper.c_str()));

								//Костыль: если оплата штрафа, опционально печатаем квитанцию 2 раза
								string sExtraCopies;
								if (i->kv_name.find("Оплата штрафа") != -1 && this->_getextra(p, sExtraCopies, "copies")) {
									nCopies = atoi(sExtraCopies.c_str());
								}
								for (int iCopies = 0; iCopies < nCopies; iCopies++) {
									if (iCopies == 1) {
										i->kv_name.append("\n(копия!!!)");
									}
									if (this->cAtol->dwPrinterModel == 0) {
										if (c->nCheckType > 0)
										{
											this->_printgibddpd4kv(p,c,dc,i);
										}
										else
											this->_printgibddkv(p,c,dc,i);
										this->Cut();
									} else {
										this->cAtol->PrintCheckSsp(p,c,dc,i);
									}

									_app->m_pCrypto->decPrinterConfig(1);
									this->_sleep();
								}
								//////////////////////////////
							}
							else
							{
								dwRet = I3E_PRINT_PRINTERERROR;
								break;
							}
						}
					}

					/*if (p->flags & CRYPTO_PAYMENT_METADATA_HAS_CHANGE) //ch print
					{
						if (this->UpdateStatus() == I3E_STATUS_OK)
						{
							_printgibddch(p,c);
							this->Cut();
							_app->m_pCrypto->decPrinterConfig(2);
							this->_sleep();
						}
					}*/
				}
			}
		}
	}
	if (dwRet != I3E_PRINT_PRINTERERROR)
	{
		if (this->UpdateStatus() != I3E_STATUS_OK)
		{
			dwRet = I3E_PRINT_PRINTERERROR;
		}
	}
	return dwRet;
}

void CPrinter::_printgibddch(PAYMENT *p, CONFIG *c)
{
	this->_prepare(I3E_CHECK_NORMAL);
	this->_set_justify(1);
	this->PrintLine(true,"");
	if (p->flags & CRYPTO_PAYMENT_METADATA_IS_DUMMY)
	{
		this->PrintLine(true,"По этому коду операции\r\nвы можете воспользоваться\r\nсуммой недоплаты для оплаты\r\nлюбой доступной услуги");
	}
	else
	{
		this->PrintLine(true,"По этому коду операции\r\nвы можете воспользоваться\r\nсуммой сдачи для оплаты\r\nлюбой доступной услуги");
	}
	this->PrintLine(true,"");
	this->PrintLine(true,"");
	this->PrintLine(false,"Номер терминала: ");
	this->_set_emphasis(1);
	this->PrintLine(true,L"%d",c->osmpId);
	this->_set_emphasis(0);
	this->PrintLine(true,"");
	this->PrintLine(true,"");
	this->PrintLine(false,"Код операции: ");
	this->_set_emphasis(1);
	this->PrintLine(true,"%I64d",p->transactionNumber);
	this->_set_emphasis(0);
	this->PrintLine(true,"");
	this->PrintLine(true,"");
	this->PrintLine(false,"Сумма: ");
	this->_set_emphasis(1);
	this->PrintLine(false,"%.2f",p->change);
	//this->PrintLine(true,"");
	this->_postprintspace(4);
	this->_set_emphasis(0);
	this->_set_justify(0);
}

bool CPrinter::PrintCashout(CASHOUT *co, CONFIG *c)
{
	bool fret = false;
	int nTotalCount = 0;
	int nTotalValue = 0;
	string sBuf;

	for (int i = 0; i < co->cbt.size(); ++i)
	{
		nTotalCount += co->cbt[i].nCount;
		nTotalValue += co->cbt[i].nValue * co->cbt[i].nCount;
	}

	FormatString(sBuf, "%d%I64d%07d", c->osmpId, co->id, nTotalValue);

	if (UpdateStatus() == I3E_STATUS_OK)
	{
		//if(bWinspool)
		_update_spooler();
		this->_prepare(I3E_CHECK_NORMAL);

		//if (this->dwPrinterModel != IEEE_PRINTER_M_STAR && this->cAtol->dwPrinterModel == 0) {
			/*int req = stringToCode128Command(sBuf);
			if (req) {
				BYTE *bBarCode = new BYTE[req];
				stringToCode128Command(sBuf, bBarCode);
				this->_set_barcode_height();
				this->_set_barcode_width();
				this->_set_justify(1);
				this->_print_barcode(bBarCode, req);
				this->_set_justify(0);
				delete[] bBarCode;
				this->PrintLine(true,"");
				this->PrintLine(true,"");
			}*/

			/*FormatString(sBuf, "%I64d;0;%d;%d", co->id, c->osmpId, nTotalValue);
			this->_print_barcode2D(sBuf);
			this->PrintLine(true,"");
		}*/

		this->PrintLine(true,"ИНКАССАЦИЯ N%I64d",co->id);
		this->PrintLine(true,"");
		this->PrintLine(true, L"ID терминала: %s", c->terminalId.c_str());
		this->PrintLine(true,"");
		this->PrintLine(true,"Номер терминала: %d",c->osmpId);
		this->PrintLine(true,"");
		this->PrintLine(true,"Дата: %02d.%02d.%04d %02d:%02d:%02d",co->datetimeS.wDay, co->datetimeS.wMonth, co->datetimeS.wYear, co->datetimeS.wHour, co->datetimeS.wMinute, co->datetimeS.wSecond);
		this->PrintLine(true,"");
		if (co->i.sName.length() != 0) {
			this->PrintLine(true,"Инкассатор: %d (%s)",co->uid, co->i.sName.c_str());
		} else {
			this->PrintLine(true,"Инкассатор: %d",co->uid);
		}
		this->PrintLine(true,"");
		for (int i = 0; i < co->cbt.size(); ++i)
		{
			this->PrintLine(true,"   Номинал %d %s: количество: %d",co->cbt[i].nValue,co->cbt[i].stName.c_str(),co->cbt[i].nCount);
		}
		this->PrintLine(true,"");
		this->PrintLine(true,"Всего купюр: %d",nTotalCount);
		this->PrintLine(true,"Сумма: %d",nTotalValue);

		this->_postprintspace(4);
		if (this->cAtol->dwPrinterModel == 0 || !c->bFisk) {
			this->Cut();
		} else {
			this->cAtol->Cashout(nTotalValue);
		}

		_app->m_pCrypto->decPrinterConfig(4);
		fret = true;
	}
	return fret;
}

void CPrinter::_printgibddkv(PAYMENT *p, CONFIG *c,DEALER_CONFIG *dc,vector<m_kv>::iterator i)
{
	string fio = "", address = "", sBuf;
	this->_getextra(p,fio,"fio");
	this->_getextra(p,address,"address");

	/*BYTE font = 0;
	font |= 1;
	font |= 16;
	font |= 32;
	this->_set_font(font);*/
	this->_prepare(I3E_CHECK_NORMAL);


	//this->PrintLine(true,"");
	this->_set_justify(1);
	this->PrintLine(true,"КВИТАНЦИЯ");
	this->_set_justify(0);
	this->PrintLine(true,"************************************************");
	this->PrintLine(true,"\"КИВИ БАНК\" (ЗАО)");
	this->PrintLine(true,"БИК 044525256 ИНН 3123011520");
	this->PrintLine(true,"К\С 30101810200000000416");
	this->PrintLine(true,"ТЕЛЕФОН: %s",dc->bank_phone.c_str());
	
	this->PrintLine(true,"");
	this->PrintLine(true,L"Терминал N %d",c->osmpId);
	this->PrintLine(false,"Код операции: ");
	this->_set_emphasis(1);
	this->PrintLine(true,"%I64d",p->transactionNumber);
	this->_set_emphasis(0);
	this->PrintLine(true,"Дата: %.2d.%.2d.%d Время: %.2d:%.2d",p->datetime.wDay,p->datetime.wMonth,p->datetime.wYear,p->datetime.wHour,p->datetime.wMinute);
	this->PrintLine(true,"************************************************");

	this->PrintLine(true,"");
	this->PrintLine(true,"Плательщик: %s",fio.c_str());
	this->PrintLine(true,"Адрес:      %s",address.c_str());


	this->PrintLine(true,"");
	this->_set_underline(1);
	this->_set_emphasis(1);
	this->PrintLine(false,"СУММА ПЛАТЕЖА   ");
	this->_set_underline(0);
	this->_set_emphasis(0);
	this->PrintLine(true,"%.2f",i->kv_summ);

	this->PrintLine(true,"");
	this->_set_underline(1);
	this->_set_emphasis(1);
	this->PrintLine(true,"ПОЛУЧАТЕЛЬ:");
	this->_set_emphasis(0);
	this->_set_underline(0);
	this->PrintLine(true,i->kv_rekv);
	this->PrintLine(true,"");
	this->_set_underline(1);
	this->_set_emphasis(1);
	this->PrintLine(true,"Вид платежа:");
	this->_set_emphasis(0);
	this->_set_underline(0);
	this->PrintLine(true,i->kv_name);
	vector<RECEIPT_EXTRA> v;
	if (_app->m_pCrypto->getReceiptExtra(v)) {
		this->PrintLine(true,"");
		for (int i = 0; i < v.size(); i++) {
			sBuf = "";
			this->_getextra(p,sBuf, (char *)v[i].field.c_str());
			if (sBuf.length() > 0) {
				this->PrintLine(true,"%s: %s", v[i].name.c_str(), sBuf.c_str());
			} //if length
		} //for
	} //if getReceiptExtra
	this->PrintLine(true,"************************************************");
	this->PrintLine(true,"");
	this->PrintLine(false,"Подпись плательщика _________________");

	this->_postprintspace();
}

void CPrinter::_printgibddpd4kv(PAYMENT *p, CONFIG *c, DEALER_CONFIG *dc, vector<m_kv>::iterator it)
{
	HANDLE hTemplate;
	string sFile;
	BYTE *bFile;
	vector<string> chk(PD4_ROWS);
	DWORD dwSize = NULL,dwReaded = NULL;
	string stTransID, stTerminal, stDateTime, stDate, stSum, stPayer, stAddress, stRekv, stName;
	int t = 0;

	this->_prepare(I3E_CHECK_PD4);

	wstring wsTpl = L"site/tpl/";
	if (c->wsTpl.length() != 0) {
		wsTpl.append(c->wsTpl);
	} else {
		wsTpl.append(PRINTER_TPL_CHECKPD4);
	}

	hTemplate = CreateFile(wsTpl.c_str(),GENERIC_READ ,FILE_SHARE_READ, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hTemplate == INVALID_HANDLE_VALUE)
	{
		_app->__debug(IEEE_DEBUG_PRINTER,0,L"Невозможно открыть файл шаблона чека ПД-4 (%s)", wsTpl.c_str());
		return;
	}
	dwSize = GetFileSize(hTemplate,0);
	if (!dwSize)
		return;
	bFile = new BYTE[dwSize + 1];
	memset(bFile,0,dwSize + 1);
	ReadFile(hTemplate,bFile,dwSize,&dwReaded,0);
	CloseHandle(hTemplate);
	if (dwReaded)
	{
		sFile = (char*)bFile;
		
		stRekv = it->kv_rekv;
		stName = it->kv_name;

		FormatString(stTransID,"%I64d",p->transactionNumber);
		t = sFile.find("%transid%");
		if (t != sFile.npos)
			sFile.replace(t,stTransID.length(),stTransID);

		FormatString(stTerminal,"%d",c->osmpId);
		t = sFile.find("%tid%");
		if (t != sFile.npos)
			sFile.replace(t,stTerminal.length(),stTerminal);
		
		FormatString(stDateTime,"%.2d.%.2d.%d %.2d:%.2d:%.2d",p->datetime.wDay,p->datetime.wMonth,p->datetime.wYear,p->datetime.wHour,p->datetime.wMinute,p->datetime.wSecond);
		t = sFile.find("%datetime%");
		if (t != sFile.npos)
			sFile.replace(t,stDateTime.length(),stDateTime);

		FormatString(stDate,"%.2d.%.2d.%d",p->datetime.wDay,p->datetime.wMonth,p->datetime.wYear);
		t = sFile.find("%date%");
		if (t != sFile.npos)
			sFile.replace(t,stDate.length(),stDate);

		double m;
		if (modf(it->kv_summ, &m) != 0) {
			FormatString(stSum,"%.2f руб.",it->kv_summ);
		} else {
			FormatString(stSum,"%.f руб.",it->kv_summ);
		}
		charToOem(stSum,stSum);
		t = sFile.find("%sum%");
		if (t != sFile.npos)
			sFile.replace(t,stSum.length(),stSum);
		t = sFile.find("%sum%");
		if (t != sFile.npos)
			sFile.replace(t,stSum.length(),stSum);

		FormatString(stPayer,"%s",p->mExtra["fio"].c_str());
		charToOem(stPayer,stPayer);
		t = sFile.find("%fio%");
		if (t != sFile.npos)
			sFile.replace(t,stPayer.length(),stPayer);

		FormatString(stAddress,"%s",p->mExtra["address"].c_str());
		charToOem(stAddress,stAddress);
		t = sFile.find("%addr%");
		if (t != sFile.npos)
		{
			sFile.erase(t,strlen("%addr%"));
			sFile.insert(t,stAddress);
			//sFile.replace(t,stAddress.length(),stAddress);
		}


		while(true)
		{
			int f = stRekv.find("\r");
			if (f != stRekv.npos)
				stRekv.replace(f,1,"");
			else
				break;
		}
		
		if (stRekv.length()> 0 && stRekv[stRekv.length()-1] != '\n')
			stRekv.append("\n");
		
		for (int i = 0; i < 5; ++i)
		{
			string bl;
			FormatString(bl,"%%bankline%d%%",i);
			int f = stRekv.find("\n");
			if (f != stRekv.npos)
			{
				string sbl = stRekv.substr(0,f);
				int _b = sFile.find(bl);
				if (_b != sFile.npos)
				{
					charToOem(sbl,sbl);
					sFile.replace(_b,bl.length(),"");
					sFile.insert(_b,sbl);
				}
				stRekv.erase(0,f+1);
			}
			else
			{
				int _b = sFile.find(bl);
				if (_b != sFile.npos)
					sFile.replace(_b,bl.length(),"");
			}
		}

		for (int i = 0; i < 4; i++)
		{
			string pt;
			FormatString(pt,"%%paytype%d%%",i);
			int f = stName.find("\n");
			if (f != stName.npos)
			{
				string spt = stName.substr(0,f);
				int _b = sFile.find(pt);
				if (_b != sFile.npos)
				{
					charToOem(spt,spt);
					sFile.replace(_b,spt.length(),spt);
					//sFile.insert(_b,spt);
				}
				stName.erase(0,f+1);
			}
			else if (stName.length() > 0)
			{
				int _b = sFile.find(pt);
				if (_b != sFile.npos)
				{
					charToOem(stName,stName);
					sFile.replace(_b,stName.length(),stName);
				}
				stName.erase(0,stName.length());
			}
			else
			{
				int _b = sFile.find(pt);
				if (_b != sFile.npos)
					sFile.replace(_b,10,"          ");
			}
		}
		

		for (int i = PD4_ROWS-1; i >= 0; --i)
		{
			int p = sFile.find("\r\n");
			if (p == sFile.npos)
				break;
			chk[i] = sFile.substr(0,p);
			sFile.erase(0,p + 2);
		}


		for (int z = 0; z < PD4_LENGTH; z++)
		{
			for (int i = 0; i < PD4_ROWS; i++)
			{
				if (chk[i].length() <= z)
				{
					this->PrintLine(false," ");
				}
				else
				{
					this->PrintLine(false,(BYTE)chk[i][z]);
				}
			}
			this->PrintLine(true,"");
		}
		this->_set_rotate(0);
		if (dc->phone.length() < PD4_ROWS) {
			this->PrintLine(true, "               " + dc->phone);
		}
		this->_postprintspace();
	}
	delete[] bFile;
}

bool CPrinter::PrintTest(CONFIG *c)
{
	bool fret = false;
	if (UpdateStatus() == I3E_STATUS_OK)
	{
		//if(bWinspool)
		_update_spooler();
		this->_prepare(I3E_CHECK_NORMAL);
		this->PrintLine(true,"     Тестовая печать");
		this->PrintLine(true,"");
		this->PrintLine(true,"Терминал: %d", c->osmpId);
		this->PrintLine(true,"");
		this->PrintLine(true,"Модель принтера: %s",this->stPrinterModel.c_str());
		this->_postprintspace(10);
		this->Cut();
		_app->m_pCrypto->decPrinterConfig(5);
		fret = true;
	}
	return fret;
}

bool CPrinter::PrintVisit(CONFIG *c, VISIT *v) {
	bool fret = false;
	if (UpdateStatus() == I3E_STATUS_OK)
	{
		//if(bWinspool)
		_update_spooler();
		this->_prepare(I3E_CHECK_NORMAL);

		this->_set_justify(1);
		this->_set_emphasis(1);
		this->PrintLine(true, "Визит техника (инкассатора)");
		this->_set_emphasis(0);
		this->_set_justify(0);
		this->PrintLine(true,"");

		this->PrintLine(true, "************Визит************");
		this->PrintLine(true, L"ID терминала: %s", c->terminalId.c_str());
		this->PrintLine(true, "Номер терминала: %d", c->osmpId);
		/*this->_set_emphasis(1);
		this->PrintLine(true,L"%d", c->osmpId);
		this->_set_emphasis(0);*/

		this->PrintLine(true, "Дата визита: %.2d.%.2d.%d %.2d:%.2d:%.2d", v->datetimeS.wDay,v->datetimeS.wMonth,v->datetimeS.wYear,v->datetimeS.wHour,v->datetimeS.wMinute,v->datetimeS.wSecond);
		/*this->_set_emphasis(1);
		PrintLine(true,"%.2d.%.2d.%d %.2d:%.2d:%.2d",v->datetimeS.wDay,v->datetimeS.wMonth,v->datetimeS.wYear,v->datetimeS.wHour,v->datetimeS.wMinute,v->datetimeS.wSecond);
		this->_set_emphasis(0);*/

		this->PrintLine(true,"Номер визита: %I64d", v->id);
		/*this->_set_emphasis(1);
		this->PrintLine(true,L"%I64d", v->id);
		this->_set_emphasis(0);*/

		this->PrintLine(true,"Номер техника: %d", v->i.uid);
		/*this->_set_emphasis(1);
		this->PrintLine(true,L"%d", v->i.uid);
		this->_set_emphasis(0);*/

		this->PrintLine(true, "Имя техника: %s", v->i.sName.c_str());
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%s", v->i.sName.c_str());
		this->_set_emphasis(0);*/

		this->PrintLine(true,"");

		this->PrintLine(true, "************Оборудование************");
		this->PrintLine(true, "Принтер:");
		this->PrintLine(true,"    Модель: %s", this->stPrinterModel.c_str());
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%s", this->stPrinterModel.c_str());
		this->_set_emphasis(0);*/

		this->PrintLine(true, "    Состояние: %s", ((this->dwPrinterStatus) ? "Неисправен" : "ОК"));
		/*this->_set_emphasis(1);
		if (this->dwPrinterStatus == 0) {
			this->PrintLine(true, "ОК");
		} else {
			this->PrintLine(true, "Неисправен!");
		}
		this->_set_emphasis(0);*/

		PRINTER_CONFIG pc;
		_app->m_pCrypto->getPrinterConfig(&pc);

		this->PrintLine(true, "    Остаток ленты: %.2f м.", (float)pc.rollSize / 1000);
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%.2f м.", (float)pc.rollSize / 1000);
		this->_set_emphasis(0);*/

		if (this->cAtol->dwPrinterModel != 0) {
			if (c->bFisk) {
				this->cAtol->PrintLine("    Фискальный режим: Да");
				if (this->cAtol->cbStatusEx.bZFreeSupported) {
					this->cAtol->PrintLine("    Z-отчеты: Ост. %d", this->cAtol->cbStatusEx.bZFree);
				} else {
					this->cAtol->PrintLine("    Z-отчеты: Не поддерж.");
				}
			} else { //if fisk
				this->cAtol->PrintLine("    Фискальный режим: Нет");
			} //if fisk
		} //if atol

		this->PrintLine(true, "Купюроприемник:");
		this->PrintLine(true,"    Модель: %s", _app->m_pCashCode->stModelName.c_str());
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%s", _app->m_pCashCode->stModelName.c_str());
		this->_set_emphasis(0);*/

		this->PrintLine(true, "    Состояние: %s", ((this->dwPrinterStatus) ? "Неисправен" : "ОК"));
		/*this->_set_emphasis(1);
		if (_app->m_pCashCode->dwLastState == 0) {
			this->PrintLine(true, "ОК");
		} else {
			this->PrintLine(true, "Неисправен!");
		}
		this->_set_emphasis(0);*/

		unsigned int nBills[CCNET_BILL_TYPES];
		ZeroMemory(&nBills, CCNET_BILL_TYPES * sizeof(unsigned int));
		_app->m_pCrypto->getCcnetStat(nBills);
		this->PrintLine(true,"    Кол-во купюр: %d", getBillCount(nBills));
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%d", getBillCount(nBills));
		this->_set_emphasis(0);*/

		this->PrintLine(true, "Сторожевой таймер: %s", ((_app->m_pWatchDog->WatchDogVersion) ? _app->m_pWatchDog->WatchDogVersionName.c_str() : "Отсутствует"));
		/*if (_app->m_pWatchDog->WatchDogVersion != 0) {
			this->PrintLine(false,"    Модель: ");
			this->_set_emphasis(1);
			this->PrintLine(true, "%s", _app->m_pWatchDog->WatchDogVersionName.c_str());
			this->_set_emphasis(0);
		} else {
			this->PrintLine(true,"    Отсутствует");
		}*/

		this->PrintLine(true, "");

		this->PrintLine(true, "************Связь************");
		this->PrintLine(true, "Платежей в очереди: %d", _app->m_pCrypto->getPaymentsCount());
		/*this->_set_emphasis(1);
		this->PrintLine(true, "%d", _app->m_pCrypto->getPaymentsCount());
		this->_set_emphasis(0);*/
		

		this->_postprintspace(10);
		this->Cut();
		_app->m_pCrypto->decPrinterConfig(5);
		fret = true;
	}
	return fret;
}

bool CPrinter::_getextra(PAYMENT *p,string &stDest,char *szName,...)
{
	bool fret = false;
	char *szBuff = 0, *szValue = 0;
	int req = 0;
	va_list ap;
	va_start (ap, szName);

	req = _vscprintf((char*)szName,ap);
	if (req)
	{
		szBuff = new char[req + 1];
		vsprintf (szBuff, (char*)szName, ap);
		if (p->mExtra.find(szBuff) != p->mExtra.end())
		{
			stDest = p->mExtra[szBuff];
			fret = true;
		}
		/*req = 0;
		if (_app->m_pCrypto->getPaymentExtraValue(p,szBuff,0,&req) == CRYPTO_POST_OK && req)
		{
			szValue = new char[req];
			_app->m_pCrypto->getPaymentExtraValue(p,szBuff,szValue,&req);
			stDest = szValue;
			fret = true;
			delete[] szValue;
		}*/
		delete[] szBuff;
	}
	va_end (ap);
	return fret;
}

void CPrinter::Cut(bool full) {
	
	if (this->cAtol->dwPrinterModel != 0) {
		this->cAtol->Cut();
		return;
	}
	BYTE eject[4] = {0x1D,0x65,3,7};

	/*if (this->dwPrinterModel == IEEE_PRINTER_M_AV268) {
		BYTE bAV[] = {0x0D, 0x0A};
		WriteComPort(hPort,bAV,sizeof(bAV));
	}*/

	if (this->dwPrinterModel == IEEE_PRINTER_M_STAR) {
		BYTE cmd[] = {0x1B,0x64, 0};
		WriteComPort(hPort,cmd,sizeof(cmd));
	} else {
		BYTE cmd[] = {0x1B,0x69};
		WriteComPort(hPort,cmd,sizeof(cmd));
	}

	if (this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMVKP80 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMTG2480)
	{
		WriteComPort(hPort,eject,4,0,0,0,0);
	}
}

void CPrinter::_put_eject(BYTE n)
{
	BYTE cmd[] = {0x1D,0x65,n};
	WriteComPort(hPort,cmd,sizeof(cmd),0,0,0,0);
}

void CPrinter::_set_cpi(BYTE n) {
	BYTE cmd[] = {0x1B,0xC1,n};
	WriteComPort(hPort,cmd,sizeof(cmd),0,0,0,0);
}

void CPrinter::PrintLine(bool bcrlf,char *szLine,...)
{
	char *szBuff = 0;
	string s;
	va_list ap;
	va_start (ap, szLine);
	int req = _vscprintf((char*)szLine,ap);
	if (req)
	{
		szBuff = new char[req + 1];
		vsprintf (szBuff, (char*)szLine, ap);
		if (this->cAtol->dwPrinterModel == 0) {
			CharToOemA(szBuff,szBuff);
			s = szBuff;
			this->_printline(bcrlf,s);
		} else {
			this->cAtol->PrintLine(szBuff);
		}
		delete[] szBuff;
	}
	else if(strlen(szLine) == 0)
	{
		if (this->cAtol->dwPrinterModel == 0) {
			s = szLine;
			this->_printline(bcrlf,s);
		} else {
			this->cAtol->PrintLine("");
		}
	}
	va_end (ap);
}

void CPrinter::PrintLine(bool bcrlf,wchar_t *szLine,...)
{
	wchar_t *szBuff = 0;
	string s;
	va_list ap;
	va_start (ap, szLine);
	int req = _vscwprintf((wchar_t*)szLine,ap);
	if (req)
	{
		szBuff = new wchar_t[req + 1];
		vswprintf(szBuff, (wchar_t*)szLine, ap);
		int nch = WideCharToMultiByte(CP_ACP,NULL,szBuff,-1,NULL,NULL,NULL,NULL);
		if (nch)
		{
			char *str = new char[nch];
			WideCharToMultiByte(CP_ACP,NULL,szBuff,-1,str,nch,NULL,NULL);
			if (this->cAtol->dwPrinterModel == 0) {
				CharToOemA(str,str);
				s = str;
				this->_printline(bcrlf,s);
			} else {
				this->cAtol->PrintLine(str);
			}
			delete[] str;
		}
		delete[] szBuff;
	}
	else if(wcslen(szLine) == 0)
	{
		
		if (this->cAtol->dwPrinterModel == 0) {
			s = "";
			this->_printline(bcrlf,s);
		} else {
			this->cAtol->PrintLine("");
		}
	}
	va_end (ap);
}

void CPrinter::PrintLine(bool bcrlf,BYTE b)
{
	char tmp[2];
	tmp[0] = b;
	tmp[1] = 0;
	//if (b <= 178 || b >= 219)
	//	CharToOemA(tmp,tmp);
	string s = tmp;
	this->_printline(bcrlf,s);
}

void CPrinter::PrintLine(bool bcrlf,std::string &s)
{
		if (this->cAtol->dwPrinterModel == 0) {
			charToOem(s,s);
			this->_printline(bcrlf,s);
		} else {
			this->cAtol->PrintLine((char *)s.c_str());
		}
}

void CPrinter::_printline(bool bcrlf,std::string &st)
{
	BYTE crlf[] = {0x0D,0x0A};

	if (st.length() != 0) {
		WriteComPort(this->hPort,(BYTE*)st.c_str(),st.length());
	}

	if (bcrlf) {
		WriteComPort(this->hPort,crlf,sizeof(crlf));
	}
}

DWORD CPrinter::_update_status() {
	this->_clearstatus();

	BYTE answer[] = {0};
	DWORD dwReaded = 0;

	PurgeComm(hPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
	if (this->dwPrinterModel == IEEE_PRINTER_M_CITIZENPPU700 || this->dwPrinterModel == IEEE_PRINTER_M_CITIZENS2000 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMVKP80 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMTG2480) {
		BYTE cmd[] = {0x10,0x04,1};
		if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0)
		{
			if (answer[0] & 8)
			{
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера OFF-LINE");
				_appendstatus(I3E_STATUS_OFFLINE);
				cmd[2] = 2;
				if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0)
				{
					if (answer[0] & 4)
						_appendstatus(I3E_STATUS_OC_COVER_OPENED);
					if (answer[0] & 8)
						_appendstatus(I3E_STATUS_OC_LINE_FEED);
					if (answer[0] & 32)
						_appendstatus(I3E_STATUS_OC_PAPER_END);
					if (answer[0] & 64)
						_appendstatus(I3E_STATUS_OC_ERROR);
				}
				cmd[2] = 3;
				if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0)
				{
					if (answer[0] & 8)
						_appendstatus(I3E_STATUS_ER_CUTTER_ERROR);
					if (answer[0] & 32)
						_appendstatus(I3E_STATUS_ER_UNRECOVERABLE);
					if (answer[0] & 64)
						_appendstatus(I3E_STATUS_ER_RECOVERABLE);
				}
				cmd[2] = 4;
				if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0)
				{
					if (answer[0] & 8)
						_appendstatus(I3E_STATUS_SR_NEAR_PAPER_END);
					if (answer[0] & 96)
						_appendstatus(I3E_STATUS_SR_PAPER_NOT_FOUND);
				}
			}
			else
			{
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера ON-LINE");
			}

			/*if (nSleep && this->dwPrinterStatus == I3E_STATUS_OFFLINE) {
				Sleep(nSleep);
				return this->UpdateStatus();
			}*/ //if need sleep
		}
		else
		{
			_appendstatus(I3E_STATUS_NOANSWER);
		}
	} else if (this->dwPrinterModel == IEEE_PRINTER_M_STAR) { //if citizen custom
		BYTE cmd[] = {0x04};
		if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0) {
			if (answer[0] & 2) {
				_appendstatus(I3E_STATUS_OC_ERROR);
			/*} else if (answer[0] & 4) {
				_appendstatus(I3E_STATUS_SR_NEAR_PAPER_END);*/
			} else if (answer[0] & 8) {
				_appendstatus(I3E_STATUS_OC_PAPER_END);
			/*} else if (answer[0] & 32) {
				_appendstatus(I3E_STATUS_SR_NEAR_PAPER_END);*/
			} else if (answer[0] & 64) {
				_appendstatus(I3E_STATUS_OC_PAPER_END);
			} else {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера ON-LINE");
			}
		} else {
			_appendstatus(I3E_STATUS_NOANSWER);
		}
	} else if (this->dwPrinterModel == IEEE_PRINTER_M_AV268) {//if star
		BYTE cmd[] = {0x1B, 0x76};
		if (WriteComPort(hPort,cmd,sizeof(cmd),true,answer,sizeof(answer),&dwReaded) && dwReaded > 0) {
			if (answer[0] & 2 || answer[0] & 32 || answer[0] & 64) {
				_appendstatus(I3E_STATUS_OC_ERROR);
			} else if (answer[0] & 4) {
				_appendstatus(I3E_STATUS_OC_PAPER_END);
			} else if (answer[0] & 8) {
				_appendstatus(I3E_STATUS_OC_COVER_OPENED);
			} else {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера ON-LINE");
			}
		} else {
			_appendstatus(I3E_STATUS_NOANSWER);
		}
	} //if av268

	return this->dwPrinterStatus;
}

DWORD CPrinter::UpdateStatus(int nSleep) {
	DWORD dwRet = 0;

	if (this->cAtol->dwPrinterModel != 0) {
		for (int i = 1; i <= 5; i++) {
			dwRet = this->cAtol->UpdateStatus();
			if (dwRet & I3E_STATUS_OFFLINE) {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера OFF-LINE, попытка %d, ожидание 1 сек", i);
				Sleep(1000);
			} else { //if offline
				break;
			}
		} //for
		this->dwPrinterStatus = dwRet;
	} else {

		for (int i = 1; i <= 5; i++) {
			dwRet = this->_update_status();
			if (dwRet & I3E_STATUS_NOANSWER || dwRet & I3E_STATUS_OFFLINE) {
				_app->__debug(IEEE_DEBUG_PRINTER,0,"Состояние принтера OFF-LINE, попытка %d, ожидание 1 сек", i);
				Sleep(1000);
			} else { //if offline
				break;
			}
		} //for
	}

	_app->SetDeviceStatus(I3E_DEVSTATUS_PRINTER,this->dwPrinterStatus);
	return dwRet;
}

void CPrinter::StatusInString(std::string &s, DWORD dwStatus)
{
	if (dwStatus == I3E_STATUS_OK)
	{
		s.append("Online; ");
	}
	else
	{
		if (dwStatus & I3E_STATUS_NOANSWER)
		{
			s.append("No answer; ");
		}
		if (dwStatus & I3E_STATUS_OFFLINE)
		{
			s.append("Offline; ");
		}
		if (dwStatus & I3E_STATUS_OC_COVER_OPENED)
		{
			s.append("Cover opened; ");
		}
		if (dwStatus & I3E_STATUS_OC_LINE_FEED)
		{
			s.append("Line feed; ");
		}
		if (dwStatus & I3E_STATUS_OC_PAPER_END)
		{
			s.append("Paper end; ");
		}
		if (dwStatus & I3E_STATUS_OC_ERROR)
		{
			s.append("Error; ");
		}
		if (dwStatus & I3E_STATUS_ER_CUTTER_ERROR)
		{
			s.append("Cutter error; ");
		}
		if (dwStatus & I3E_STATUS_ER_UNRECOVERABLE)
		{
			s.append("Unrecoverable; ");
		}
		if (dwStatus & I3E_STATUS_ER_RECOVERABLE)
		{
			s.append("Recoverable; ");
		}
		if (dwStatus & I3E_STATUS_SR_NEAR_PAPER_END)
		{
			s.append("Near paper end; ");
		}
		if (dwStatus & I3E_STATUS_SR_PAPER_NOT_FOUND)
		{
			s.append("Paper not found; ");
		}
	}
}

void CPrinter::_clearstatus()
{
	this->dwPrinterStatus = I3E_STATUS_OK;
}

void CPrinter::_appendstatus(DWORD dwStatus)
{
	_app->__debug(IEEE_DEBUG_PRINTER,0,"Добавлен статус %d",dwStatus);
	this->dwPrinterStatus |= dwStatus;
}

void CPrinter::_set_underline(BYTE byte)
{
	BYTE b[3] = {0x1B,0x2D,byte};
	WriteComPort(hPort,b,3,0,0,0,0);
}

void CPrinter::_set_emphasis(BYTE byte)
{
	if (this->dwPrinterModel == IEEE_PRINTER_M_CITIZENPPU700 || this->dwPrinterModel == IEEE_PRINTER_M_CITIZENS2000 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMVKP80 || this->dwPrinterModel == IEEE_PRINTER_M_AV268 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMTG2480) {
		BYTE b[] = {0x1B,0x45,byte};
		WriteComPort(hPort,b,sizeof(b),0,0,0,0);
	} else if (this->dwPrinterModel == IEEE_PRINTER_M_STAR) {
		BYTE b[] = {0x1B,0x45};
		b[1] += byte;
		WriteComPort(hPort,b,sizeof(b),0,0,0,0);
	}
}

void CPrinter::_set_justify(BYTE byte)
{
	BYTE b[3] = {0x1B,0x61,byte};
	WriteComPort(hPort,b,3,0,0,0,0);
}

void CPrinter::_set_printmode(BYTE byte)
{
	BYTE b[3] = {0x1B,0x21,byte};
	WriteComPort(hPort,b,3,0,0,0,0);
}

void CPrinter::_set_charset(BYTE byte)
{
	BYTE b[3] = {0x1B,0x74,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_set_linefeed(BYTE byte)
{
	BYTE b[3] = {0x1B,0x33,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_set_spacing(BYTE byte)
{
	BYTE b[3] = {0x1B,0x20,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_set_font(BYTE byte)
{
	BYTE b[3] = {0x1B,0x4D,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_set_smooth(BYTE byte)
{
	BYTE b[3] = {0x1B,0x62,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_set_leftmargin(BYTE byte1,BYTE byte2)
{
	BYTE b[4] = {0x1D,0x4C,byte1,byte2};
	WriteComPort(hPort,b,4);
}

void CPrinter::_set_rotate(BYTE byte)
{
	BYTE b[] = {0x1B,0x56,byte};
	WriteComPort(hPort,b,3);
}

void CPrinter::_prepare(DWORD dwCheckType)
{
	this->_put_init();
	
	if (this->dwPrinterModel == IEEE_PRINTER_M_CITIZENS2000 || this->dwPrinterModel == IEEE_PRINTER_M_CITIZENPPU700 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMVKP80 || this->dwPrinterModel == IEEE_PRINTER_M_CUSTOMTG2480) {
		this->_set_spacing(0);
		if (dwCheckType == I3E_CHECK_PD4) {
			this->_set_rotate(1);
		} else {
			this->_set_rotate(0);
		}
	}

	switch(this->dwPrinterModel) {
		case IEEE_PRINTER_M_CITIZENS2000:
		case IEEE_PRINTER_M_CITIZENPPU700:
			{
				this->_set_charset(7);
				if (dwCheckType == I3E_CHECK_PD4) {
					this->_set_linefeed(0);
				} else {
					this->_set_linefeed(0x29);
				}
				this->_set_printmode(0);
			}
			break;
		case IEEE_PRINTER_M_CUSTOMVKP80:
		case IEEE_PRINTER_M_CUSTOMTG2480:
			{
				//this->_put_eject(2);
				this->_set_charset(/*0*/17);
				//this->_set_cpi(1);
				//this->_set_font(1);
				this->_set_printmode(1);
				if (dwCheckType == I3E_CHECK_PD4) {
					this->_set_linefeed(0x29);
				} else {
					this->_set_leftmargin(10,0);
					this->_set_linefeed(0x37);
				}

				this->_put_custom_secret();
			}
			break;
		case IEEE_PRINTER_M_STAR:
			this->_set_font(0);

			BYTE bStar1[] = {0x1B, 0x7A, 1};
			WriteComPort(this->hPort, bStar1, sizeof(bStar1));

			BYTE bStar2[] = {0x1B, 0x1D, 0x74, 0x0A};
			WriteComPort(this->hPort, bStar2, sizeof(bStar2));
			break;
	} //switch
}

void CPrinter::_postprintspace(int inc) {
	/*if (this->cAtol->dwPrinterModel != 0) {
		return;
	}*/

	int n = 0;
	switch(this->dwPrinterModel)
	{
	case IEEE_PRINTER_M_CITIZENPPU700:
	case IEEE_PRINTER_M_CITIZENPPU700_F:
		n = 6 + inc;
		break;
	case IEEE_PRINTER_M_CITIZENS2000:
	case IEEE_PRINTER_M_CITIZENS2000_F:
		n = 6 + inc;
		break;
	case IEEE_PRINTER_M_CUSTOMVKP80:
	case IEEE_PRINTER_M_CUSTOMTG2480:
	//case IEEE_PRINTER_M_CUSTOMVKP80_F:
		n = inc;
		break;
	case IEEE_PRINTER_M_AV268:
		n = 2 + inc;
		break;
	case IEEE_PRINTER_M_STAR:
		n = inc + 2;
		break;
	}

	if (n < 0) {
		n = 0;
	}
	//_app->__debug(0,0,L"postprint %d lines", n);
	for (int i = 0; i < n; i++) {
		this->PrintLine(true," ");
	}
}



void CPrinter::_put_init() {
	if (this->dwPrinterModel == IEEE_PRINTER_M_AV268) {
		return;
	}

	if (this->dwPrinterModel == IEEE_PRINTER_M_STAR) {
		BYTE bCancel[] = {0x18};
		WriteComPort(this->hPort,bCancel,sizeof(bCancel));
	}
	BYTE init[] = {0x1B,0x40};
	WriteComPort(this->hPort,init,sizeof(init));
}

DWORD CPrinter::GetStatus()
{
	return this->dwPrinterStatus;
}

DWORD CPrinter::UpdateStatusOutside()
{
	DWORD dwRet = I3E_STATUS_OFFLINE, dwMutexRet;
	dwMutexRet = WaitForSingleObject(this->hMtxPrinter,I3E_PRINTER_WAITFORPRINTER);
	if (dwMutexRet == WAIT_TIMEOUT)
	{
		_app->__debug(IEEE_DEBUG_PRINTER,0,"Фоновый опрос статуса принтера: прервано по таймауту");
	}
	else if (dwMutexRet == WAIT_OBJECT_0)
	{
		this->UpdateStatus();
		dwRet = this->dwPrinterStatus;
	}
	ReleaseMutex(this->hMtxPrinter);
	return dwRet;
}

void CPrinter::_put_custom_secret() {
	if (this->stPrinterROM != "2.28") return;

	BYTE bCmd1[] = {0x10, 0x04, 0x14};
	BYTE bCmd2[] = {0x1c, 0xe2, 0x00, 0x0d, 0x01};
	BYTE bCmd3[] = {0x1c, 0x24, 0x52};
	BYTE bCmd4[] = {0x1c, 0x24, 0x57, 0, 0, 0, 0};

	BYTE bBuff[128] = {0};
	DWORD dwRead = 0;
	WriteComPort(this->hPort,bCmd1,sizeof(bCmd1), true, bBuff, sizeof(bBuff), &dwRead);
	ZeroMemory(bBuff, sizeof(bBuff));
	WriteComPort(this->hPort,bCmd2,sizeof(bCmd2), true, bBuff, sizeof(bBuff), &dwRead);
	ZeroMemory(bBuff, sizeof(bBuff));
	WriteComPort(this->hPort,bCmd3,sizeof(bCmd3), true, bBuff, sizeof(bBuff), &dwRead);

	custom(&bBuff[0], &bBuff[1], &bBuff[2], &bBuff[3]);

	bCmd4[3] = bBuff[0];
	bCmd4[4] = bBuff[1];
	bCmd4[5] = bBuff[2];
	bCmd4[6] = bBuff[3];

	WriteComPort(this->hPort,bCmd4,sizeof(bCmd4), true, bBuff, sizeof(bBuff), &dwRead);
}

void CPrinter::_set_barcode_height(BYTE bHeight) {
	BYTE bBarCodeHeidht[] = {0x1D, 0x68, 0};
	bBarCodeHeidht[2] = bHeight;
	WriteComPort(this->hPort,bBarCodeHeidht, sizeof(bBarCodeHeidht));
}

void CPrinter::_set_barcode_width(BYTE bWidth) {
	BYTE bBarCodeWidth[] = {0x1D, 0x77, 0};
	bBarCodeWidth[2] = bWidth;
	WriteComPort(this->hPort,bBarCodeWidth, sizeof(bBarCodeWidth));
}

void CPrinter::_print_barcode(BYTE *bBarCode, int nLen) {
	WriteComPort(hPort,bBarCode, nLen);
}

void CPrinter::_sleep() {
	if (this->cAtol->dwPrinterModel == 0) {
		Sleep(IEEE_PRINTER_PRINT_SLEEP);
	} else {
		Sleep(IEEE_PRINTER_PRINT_SLEEP_F);
	}
	/*if (this->dwPrinterModel == IEEE_PRINTER_M_CITIZENPPU700) {
		Sleep(IEEE_PRINTER_PRINT_SLEEP_PPU);
	}*/
}

void CPrinter::_print_barcode2D(wstring wsText) {
	string sText;
	unicodeToAnsi_s(wsText, sText);
	this->_print_barcode2D(sText);
}

void CPrinter::_print_barcode2D(string sText) {
	BYTE szSetBit[] = {0x1B,0x2A,0,128,0};
	BYTE szSetInt[] = {0x1B,0x33,0};

	QRcode *qrcode;

	qrcode = QRcode_encodeString(sText.c_str(),0,QR_ECLEVEL_L,QR_MODE_KANJI,1);
	if (!qrcode) {
		_app->__debug(IEEE_DEBUG_PRINTER,0,"2-мерный штрих-код: Слишком длинная строка,  %d символов", sText.c_str());
	}

	unsigned char *p;
	int x, y;
	int pix = 2;

	int bytesneed = ((qrcode->width*pix%8)>0?qrcode->width*pix/8+1:qrcode->width*pix/8)*qrcode->width*pix;
	BYTE * vv = new BYTE[bytesneed];
	ZeroMemory(vv, bytesneed);

	p = qrcode->data;
	for(y = 0; y < qrcode->width; y++) {
		for(x = 0; x < qrcode->width; x++) {
			int byte = ((y*pix/8)*qrcode->width + x)*pix;
			int bit = pow(float(2),7-y*pix%8);
			if (*p & 1) {
				for (int yy = 0; yy < pix; yy++) {
					for (int xx = 0; xx < pix; xx++) {
						vv[byte + xx] |= bit;
					} //for xx
					bit = bit >> 1;
					if (bit == 0) {
						byte += qrcode->width * pix;
						bit = 128;
					} //if bit = 0
				} //for yy
			} //if & 1
			p++;
		} //for x
	} //for y

	WriteComPort(this->hPort,szSetInt, sizeof(szSetInt));

	for (int i = 0; i < bytesneed / (qrcode->width * pix); i++) {
		PrintLine(false,"            ");

		szSetBit[4] = qrcode->width * pix / 256;
		szSetBit[3] = qrcode->width * pix % 256;

		WriteComPort(this->hPort,szSetBit, sizeof(szSetBit));

		BYTE *line = new BYTE[qrcode->width * pix];
		memcpy(line, vv + i*qrcode->width*pix, qrcode->width*pix);
		WriteComPort(this->hPort,line,qrcode->width*pix);

		PrintLine(true,"");

		delete[] line;
	}

	delete[] vv;
}

void CPrinter::_update_spooler(void)
{
	return;
}