﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Models;
using utils;

/// <summary>
/// Summary description for Pay
/// </summary>
public class Pay
{
	SqlDatabase db;
	DataClassesDataContext dc;
	provider p;
	string resp;
	request rq;
	response rs;
	string[] megatec_errors;

	public Pay (request req)
	{
		db = DatabaseFactory.CreateDatabase () as SqlDatabase;
		dc = new DataClassesDataContext ();
		p = dc.providers.Where (pp => pp.id == req.prv_id).FirstOrDefault ();
		rq = req;
		rs = new response ();
		megatec_errors = new string[7];
		megatec_errors [0] = "операция выполнена успешно, платеж проведен";
		megatec_errors [1] = "заявка не найдена в БД";
		megatec_errors [2] = "внутренняя ошибка при создании платежа";
		megatec_errors [3] = "заявка была аннулирована";
		megatec_errors [4] = "путевка уже оплачена";
		megatec_errors [6] = "некорректный или устаревший идентификатор аутентификации(token)";
	}

	public response check ()
	{
		int result;
		string comment;
		string url;
		int pay_type;
		int log_id;
		int soap;
		int gateway_id;
		string login;
		string password;

		try {
			if (db != null) {

				DbCommand dbCommand = db.GetStoredProcCommand (rq.proc_name);

				db.AddInParameter (dbCommand, "txn_id", DbType.Int64, rq.txn_id);
				db.AddInParameter (dbCommand, "sum", DbType.Currency, rq.sum);
				if (rq.prv_id > 0) {
					db.AddInParameter (dbCommand, "prv_id", DbType.Int32, rq.prv_id);
				}
				if (rq.pay_type > 0) {
					db.AddInParameter (dbCommand, "pay_type", DbType.Int32, rq.pay_type);
				}
				db.AddInParameter (dbCommand, "trm_id", DbType.Int64, rq.trm_id);
				db.AddInParameter (dbCommand, "login", DbType.String, rq.login);
				db.AddInParameter (dbCommand, "request", DbType.Xml, rq.nvc.ToXml ());
				db.AddParameter (dbCommand, "account", DbType.String, ParameterDirection.InputOutput, "account", DataRowVersion.Current, rq.account);
				db.AddOutParameter (dbCommand, "result", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "comment", DbType.String, 255);
				db.AddOutParameter (dbCommand, "url", DbType.String, 255);
				db.AddOutParameter (dbCommand, "pt", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "log_id", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "soap", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "gateway_id", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "soap_login", DbType.String, 255);
				db.AddOutParameter (dbCommand, "soap_password", DbType.String, 255);

				db.ExecuteNonQuery (dbCommand);

				rq.account = (string)db.GetParameterValue (dbCommand, "account");//Сделали возвращаемый акаунт из хранимки
				result = (int)db.GetParameterValue (dbCommand, "result");
				comment = (string)db.GetParameterValue (dbCommand, "comment");
				url = db.GetParameterValue (dbCommand, "url") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "url");
				pay_type = db.GetParameterValue (dbCommand, "pt") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "pt");
				log_id = db.GetParameterValue (dbCommand, "log_id") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "log_id");
				soap = db		.GetParameterValue (dbCommand, "soap") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "soap");
				gateway_id = db.GetParameterValue (dbCommand, "gateway_id") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "gateway_id");
				login = db.GetParameterValue (dbCommand, "soap_login") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "soap_login");
				password = db.GetParameterValue (dbCommand, "soap_password") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "soap_password");

				if (result > 0) {
					rs.osmp_txn_id = rq.txn_id;
					rs.result = result;
					rs.comment = comment;
				} else if (string.IsNullOrEmpty (url)) {
					/*
					rs.osmp_txn_id = rq.txn_id;
					rs.result = result;
					rs.comment = comment;
					rs.sum = rq.sum;
					if (rq.sum == 10.00)
					{
						rs.fields = "<fields><field1>test</field1><field2>test</field2><field3>test</field3><field4>15.00</field4></fields>";
					}
					*/
					rs.osmp_txn_id = rq.txn_id;
					rs.result = 7;
					rs.comment = "Прием платежа запрещен провайдером";
				} else if (soap == 1) {
					if (gateway_id == 1) {
						check_pegas (url);
					} else if (gateway_id == 2) {
						check_megatec (url, login, password);
					}
					check_response (log_id);
				} else {
					check_request (pay_type, url);
					check_response (log_id);
				}

				if (dbCommand.Connection != null) {
					if (dbCommand.Connection.State == ConnectionState.Open) {
						dbCommand.Connection.Close ();
					}
				}
			} else {
				rs.osmp_txn_id = rq.txn_id;
				rs.result = 300;
				rs.comment = "Другая ошибка провайдера";
			}
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 300;
			rs.comment = "Другая ошибка провайдера";
		}

		return rs;
	}

	private void check_response (int log_id)
	{
		DbCommand dbCommand;
		if (rq.proc_name == "check_request") {
			dbCommand = db.GetStoredProcCommand ("check_response");
			db.AddInParameter (dbCommand, "trm_id", DbType.Int64, rq.trm_id);
		} else {
			dbCommand = db.GetStoredProcCommand ("paycheck_response");
		}

		db.AddInParameter (dbCommand, "txn_id", DbType.Int64, rs.osmp_txn_id);
		db.AddInParameter (dbCommand, "log_id", DbType.Int32, log_id);
		db.AddInParameter (dbCommand, "filial", DbType.String, rs.filial);
		db.AddParameter (dbCommand, "result", DbType.Int32, -1, ParameterDirection.InputOutput, false, 0, 0, null, DataRowVersion.Default, rs.result);
		db.AddParameter (dbCommand, "comment", DbType.String, 255, ParameterDirection.InputOutput, false, 0, 0, null, DataRowVersion.Default, rs.comment);
		db.AddOutParameter (dbCommand, "prv_id", DbType.Int32, -1);

		db.ExecuteNonQuery (dbCommand);

		rs.result = (int)db.GetParameterValue (dbCommand, "result");
		rs.comment = (string)db.GetParameterValue (dbCommand, "comment");

		if (!string.IsNullOrEmpty (rs.fields)) {
			fields fs = fields.deserialize (rs.fields);
			fs.field5 = db.GetParameterValue (dbCommand, "prv_id").ToString ();
			rs.fields = fs.serialize ();
		} else {
			fields fs = new fields ();
			fs.field5 = db.GetParameterValue (dbCommand, "prv_id").ToString ();
			rs.fields = fs.serialize ();
		}
	}

	private void check_request (int pay_type, string url)
	{
		//foreach (var provider_url in p.billing_provider_urls)
		//{
		try {
			String uri;
			if (rq.prv_id == 100187 || rq.prv_id == 100259 || rq.prv_id == 100260) {
				uri = String.Format ("{0}?command=check&txn_id={1}&account={2}&sum={3}&trm_id={4}&pay_type={5}&term_city={7}{6}", url, rq.txn_id_str, HttpUtility.UrlEncode (rq.account), rq.sum_str, rq.trm_id_str, pay_type, rq.other_params (), getCity ());
			} else {
				uri = String.Format ("{0}?command=check&txn_id={1}&account={2}&sum={3}&trm_id={4}&pay_type={5}{6}", url, rq.txn_id_str, HttpUtility.UrlEncode (rq.account), rq.sum_str, rq.trm_id_str, pay_type, rq.other_params ());
			}
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback (ValidateServerCertificate);
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (uri);
			request.Method = "GET";
			request.Timeout = 60000;
			request.ReadWriteTimeout = 60000;
			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse ()) {
				using (Stream stream = HttpHelper.copy (response.GetResponseStream ())) {
					stream.Seek (0, SeekOrigin.Begin);
					StreamReader sr = new StreamReader (stream);
					resp = sr.ReadToEnd ();

					dc.request_response_save (rq.txn_id, uri, resp);

					stream.Seek (0, SeekOrigin.Begin);
					XmlSerializer xs = new XmlSerializer (typeof(response));
					rs = (response)xs.Deserialize (stream);
					if (rs.osmp_txn_id == 0) {
						rs.osmp_txn_id = rq.txn_id;
					}
					rs.sum = rq.sum;
					if (!string.IsNullOrEmpty (rs.fields)) {
						dc.fields_save (rs.osmp_txn_id, rs.fields);
						if (rq.prv_id == 100145) {
							dc.pegas_stoimost_save (rq.prv_id, rq.account, rs.fields);
						}
					}
				}
			}
		} catch (WebException e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 1;
			rs.comment = "Временная ошибка. Повторите запрос позже";
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 300;
			rs.comment = "Другая ошибка провайдера";
		}
		//}
	}

	private string getCity ()
	{
		var city = dc.terminals.Where (t => t.terminal_id == rq.trm_id).Select (t => t.city).FirstOrDefault ();
		if (city != null)
			return HttpUtility.UrlEncode (city);
		return HttpUtility.UrlEncode ("не известно");
	}

	private void check_pegas (string url)
	{
		try {
			WSPAYService c = new WSPAYService ();
			c.Url = url;
			t_reservationDataReq rdq = new t_reservationDataReq ();
			rdq.ReservationNumber = int.Parse (rq.account);
			if (string.IsNullOrEmpty (rq.nvc.Properties ["pass"])) {
				rdq.PassportNumber = "null";//rq.data1;
			} else {
				rdq.PassportNumber = rq.nvc.Properties ["pass"];
			}
			t_reservationDataRes rds = c.getReservationData (rdq, rq.txn_id);
			rs.result = 0;
			rs.osmp_txn_id = rq.txn_id;
			rs.sum = rq.sum;
			rs.filial = rds.reservationData.OwnerINN;
			fields f = new fields ();
			f.field1 = rds.reservationData.Town;
			f.field2 = "-";
			f.field3 = rds.reservationData.Tourist;
			f.field4 = rds.reservationData.PayDebt.ToString (CultureInfo.InvariantCulture);
			rs.fields = f.serialize ();
			dc.fields_save (rs.osmp_txn_id, rs.fields);
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.result = 299;
			rs.osmp_txn_id = rq.txn_id;
			rs.comment = e.Message;
		}
	}

	private void check_megatec (string url, string login, string password)
	{
		try {
			FinanceService c = new FinanceService ();
			c.Url = url;
			TokenResult r = c.GetUserToken (login, password);
			AmountSum s = c.CheckReservation (r.Token, rq.account, rq.txn_id);
			if (s.ErrorCode == 0) {
				rs.result = 0;
				rs.osmp_txn_id = rq.txn_id;
				rs.sum = rq.sum;
				fields f = new fields ();
				f.field1 = "-";
				f.field2 = "-";
				f.field3 = "-";
				f.field4 = s.NationalCurrencyPrice.ToString (CultureInfo.InvariantCulture);
				rs.fields = f.serialize ();
				rs.filial = s.FilialKey.ToString ();
				dc.fields_save (rs.osmp_txn_id, rs.fields);
			} else {
				rs.result = s.ErrorCode;
				rs.osmp_txn_id = rq.txn_id;
				rs.sum = rq.sum;
				rs.comment = megatec_errors [s.ErrorCode];
			}
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.result = 299;
			rs.osmp_txn_id = rq.txn_id;
			rs.comment = e.Message;
		}
	}

	private static void authorize_pegas (out SecurityHeaderType s)
	{
		DateTime now = DateTime.Now.ToUniversalTime ();
		SHA1 sha = new SHA1CryptoServiceProvider ();
		string password = Convert.ToBase64String (sha.ComputeHash (Encoding.ASCII.GetBytes (string.Format ("{0}{1}{2}", now.Ticks.ToString (), now.ToString ("yyyy-MM-ddTHH:mm:ssZ"), "gv45Wgw"))));
		UsernameTokenType u = new UsernameTokenType ();
		u.Username = new AttributedString { Value = "contact" };
		u.Password = new PasswordString { Value = password, Type = "PasswordDigest" };
		u.Nonce = new EncodedString { Value = Convert.ToBase64String (Encoding.ASCII.GetBytes (now.Ticks.ToString ())) };
		u.Created = new AttributedDateTime { Value = now.ToString ("yyyy-MM-ddTHH:mm:ssZ") };
		s = new SecurityHeaderType ();
		s.UsernameToken = u;
	}

	public response pay ()
	{
		int result;
		string comment;
		string url;
		int pay_type;
		int log_id;
		int soap;
		int gateway_id;
		string login;
		string password;

		try {
			if (db != null) {

				DbCommand dbCommand = db.GetStoredProcCommand (rq.proc_name);

				db.AddInParameter (dbCommand, "txn_id", DbType.Int64, rq.txn_id);
				db.AddInParameter (dbCommand, "txn_date", DbType.DateTime, rq.txn_date);
				db.AddInParameter (dbCommand, "sum", DbType.Currency, rq.sum);
				if (rq.prv_id > 0) {
					db.AddInParameter (dbCommand, "prv_id", DbType.Int32, rq.prv_id);
				}
				if (rq.pay_type > 0) {
					db.AddInParameter (dbCommand, "pay_type", DbType.Int32, rq.pay_type);
				}
				db.AddInParameter (dbCommand, "trm_id", DbType.Int64, rq.trm_id);
				db.AddInParameter (dbCommand, "login", DbType.String, rq.login);
				db.AddInParameter (dbCommand, "request", DbType.Xml, rq.nvc.ToXml ());
				db.AddParameter (dbCommand, "account", DbType.String, ParameterDirection.InputOutput, "account", DataRowVersion.Current, rq.account);
				db.AddOutParameter (dbCommand, "result", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "comment", DbType.String, 255);
				db.AddOutParameter (dbCommand, "url", DbType.String, 255);
				db.AddOutParameter (dbCommand, "pt", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "log_id", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "soap", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "gateway_id", DbType.Int32, -1);
				db.AddOutParameter (dbCommand, "soap_login", DbType.String, 255);
				db.AddOutParameter (dbCommand, "soap_password", DbType.String, 255);

				db.ExecuteNonQuery (dbCommand);

				rq.account = (string)db.GetParameterValue (dbCommand, "account");
				result = (int)db.GetParameterValue (dbCommand, "result");
				comment = (string)db.GetParameterValue (dbCommand, "comment");
				url = db.GetParameterValue (dbCommand, "url") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "url");
				pay_type = db.GetParameterValue (dbCommand, "pt") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "pt");
				log_id = db.GetParameterValue (dbCommand, "log_id") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "log_id");
				soap = db.GetParameterValue (dbCommand, "soap") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "soap");
				gateway_id = db.GetParameterValue (dbCommand, "gateway_id") == DBNull.Value ? 0 : (int)db.GetParameterValue (dbCommand, "gateway_id");
				login = db.GetParameterValue (dbCommand, "soap_login") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "soap_login");
				password = db.GetParameterValue (dbCommand, "soap_password") == DBNull.Value ? string.Empty : (string)db.GetParameterValue (dbCommand, "soap_password");

				if (result > 0) {
					rs.osmp_txn_id = rq.txn_id;
					rs.result = result;
					rs.comment = comment;
				} else if (string.IsNullOrEmpty (url)) {
					/*
					rs.osmp_txn_id = rq.txn_id;
					rs.prv_txn = rq.txn_id;
					rs.result = result;
					rs.comment = comment;
					rs.sum = rq.sum;
					*/
					rs.osmp_txn_id = rq.txn_id;
					rs.result = 7;
					rs.comment = "Прием платежа запрещен провайдером";
				} else if (soap == 1) {
					if (gateway_id == 1) {
						pay_pegas (url);
					} else if (gateway_id == 2) {
						pay_megatec (url, login, password);
					}
					pay_response (log_id);
				} else {
					pay_request (pay_type, url);
					pay_response (log_id);
				}


				if (dbCommand.Connection != null) {
					if (dbCommand.Connection.State == ConnectionState.Open) {
						dbCommand.Connection.Close ();
					}
				}
			} else {
				rs.osmp_txn_id = rq.txn_id;
				rs.result = 300;
				rs.comment = "Другая ошибка провайдера";
			}
			/*
							scope.Complete();
						}
			*/
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 300;
			rs.comment = "Другая ошибка провайдера";
		}

		return rs;
	}

	private void pay_response (int log_id)
	{
		DbCommand dbCommand = db.GetStoredProcCommand ("pay_response");

		db.AddInParameter (dbCommand, "txn_id", DbType.Int64, rs.osmp_txn_id);
		db.AddInParameter (dbCommand, "prv_txn", DbType.Int64, rs.prv_txn);
		db.AddInParameter (dbCommand, "log_id", DbType.Int64, log_id);
		db.AddInParameter (dbCommand, "filial", DbType.String, rs.filial);
		db.AddParameter (dbCommand, "result", DbType.Int32, -1, ParameterDirection.InputOutput, false, 0, 0, null, DataRowVersion.Default, rs.result);
		db.AddParameter (dbCommand, "comment", DbType.String, 255, ParameterDirection.InputOutput, false, 0, 0, null, DataRowVersion.Default, rs.comment);

		db.ExecuteNonQuery (dbCommand);
	}

	private void pay_request (int pay_type, string url)
	{
		//foreach (var provider_url in p.billing_provider_urls)
		//{
		try {
			String uri;
			if (rq.prv_id == 100187 || rq.prv_id == 100259 || rq.prv_id == 100260) {
				uri = string.Format ("{0}?command=pay&txn_id={1}&txn_date={2}&account={3}&sum={4}&trm_id={5}&pay_type={6}&term_city={8}{7}", url, rq.txn_id_str, rq.txn_date_str, HttpUtility.UrlEncode (rq.account), rq.sum_str, rq.trm_id_str, pay_type, rq.other_params (), getCity ());
			} else {
				uri = string.Format ("{0}?command=pay&txn_id={1}&txn_date={2}&account={3}&sum={4}&trm_id={5}&pay_type={6}{7}", url, rq.txn_id_str, rq.txn_date_str, HttpUtility.UrlEncode (rq.account), rq.sum_str, rq.trm_id_str, pay_type, rq.other_params ());
			}
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback (ValidateServerCertificate);
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create (uri);
			request.Method = "GET";
			request.Timeout = 60000;
			request.ReadWriteTimeout = 60000;
			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse ()) {
				using (Stream stream = HttpHelper.copy (response.GetResponseStream ())) {
					stream.Seek (0, SeekOrigin.Begin);
					StreamReader sr = new StreamReader (stream);
					resp = sr.ReadToEnd ();

					dc.request_response_save (rq.txn_id, uri, resp);

					stream.Seek (0, SeekOrigin.Begin);
					XmlSerializer xs = new XmlSerializer (typeof(response));
					rs = (response)xs.Deserialize (stream);
					if (rs.osmp_txn_id == 0) {
						rs.osmp_txn_id = rq.txn_id;
					}
					rs.sum = rq.sum;
				}
			}
		} catch (WebException e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 1;
			rs.comment = "Временная ошибка. Повторите запрос позже";
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.osmp_txn_id = rq.txn_id;
			rs.result = 300;
			rs.comment = "Другая ошибка провайдера";
		}
		//}
	}

	private void pay_pegas (string url)
	{
		try {
			WSPAYService c = new WSPAYService ();
			c.Url = url;
			t_reservationPayReq rprq = new t_reservationPayReq ();
			rprq.ReservationNumber = int.Parse (rq.account);
			if (string.IsNullOrEmpty (rq.nvc.Properties ["pass"])) {
				rprq.PassportNumber = "null";//rq.data1;
			} else {
				rprq.PassportNumber = rq.nvc.Properties ["pass"];
			}
			rprq.PaySum = rq.sum;
			rprq.TransactionId = rq.txn_id_str;
			t_reservationPayRes rprs = c.payReservation (rprq, rq.txn_id);
			rs.result = 0;
			rs.osmp_txn_id = rq.txn_id;	// long.Parse(rprs.invoiceData.TransactionId);
			rs.prv_txn = rprs.invoiceData.Id;
			rs.sum = rprs.invoiceData.PaySum;
			rs.filial = rprs.invoiceData.OwnerINN;
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.result = 300;
			rs.osmp_txn_id = rq.txn_id;
			rs.comment = e.Message;
		}
	}

	private void pay_megatec (string url, string login, string password)
	{
		try {
			FinanceService c = new FinanceService ();
			c.Url = url;
			TokenResult r = c.GetUserToken (login, password);
			int ErrorCode = c.CreatePayment (r.Token, rq.txn_date, rq.sum, "RUR", rq.account, rq.txn_id_str, rq.txn_id);
			if (ErrorCode == 0) {
				rs.result = 0;
				rs.osmp_txn_id = rq.txn_id;
				rs.prv_txn = rq.txn_id;
				rs.sum = rq.sum;
			} else {
				rs.result = ErrorCode;
				rs.osmp_txn_id = rq.txn_id;
				rs.sum = rq.sum;
				rs.comment = megatec_errors [ErrorCode];
			}
		} catch (Exception e) {
			ErrorHandlerHelper.LogError (e);
			rs.result = 299;
			rs.osmp_txn_id = rq.txn_id;
			rs.comment = e.Message;
		}
	}

	public static bool ValidateServerCertificate (
		object sender,
		X509Certificate certificate,
		X509Chain chain,
		SslPolicyErrors sslPolicyErrors)
	{
		return true;
	}
}
