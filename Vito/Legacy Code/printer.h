#pragma once
#include <windows.h>
#include <map>
#include <string>
#include <sstream>

#include "atol.h"

using namespace std;

#define IEEE_PRINTER_M_CITIZENPPU700				1
#define IEEE_PRINTER_M_CITIZENS2000					2
#define IEEE_PRINTER_M_CUSTOMVKP80					3
#define IEEE_PRINTER_M_STAR									4
#define IEEE_PRINTER_M_AV268								5
#define IEEE_PRINTER_M_CITIZENPPU700_F				6
#define IEEE_PRINTER_M_CITIZENS2000_F				7
#define IEEE_PRINTER_M_CUSTOMVKP80_F				8
#define IEEE_PRINTER_M_CUSTOMTG2480					9


#define IEEE_PRINTER_EV_START_PRINT_PREV		L"evPrinterStartPrintPrev"
#define IEEE_PRINTER_EV_START_PRINT				L"evPrinterStartPrint"
#define IEEE_PRINTER_EV_START_CASHOUT			L"evPrinterStartCashout"
#define IEEE_PRINTER_EV_START_PRINT_TEST			L"evPrinterStartTest"
#define IEEE_PRINTER_EV_START_PRINT_VISIT			L"evPrinterStartVisit"

#define I3E_PRINTER_MUTEX_OPERATION				L"muxPrinterOperation"
#define I3E_PRINTER_WAITFORPRINTER				10000

#define PRINTER_TPL_CHECKPD4					L"pd4.bin"
#define PD4_ROWS								23
#define PD4_LENGTH								103

#define I3E_STATUS_OK							0
#define I3E_STATUS_NOANSWER						1
#define I3E_STATUS_OFFLINE						2
#define I3E_STATUS_OC_COVER_OPENED				4
#define I3E_STATUS_OC_LINE_FEED					8
#define I3E_STATUS_OC_PAPER_END					16
#define I3E_STATUS_OC_ERROR						32
#define I3E_STATUS_ER_CUTTER_ERROR				64
#define I3E_STATUS_ER_UNRECOVERABLE				128
#define I3E_STATUS_ER_RECOVERABLE				256
#define I3E_STATUS_SR_NEAR_PAPER_END			512
#define I3E_STATUS_SR_PAPER_NOT_FOUND			1024

#define IEEE_PRINTER_PRINT_SLEEP				6000
#define IEEE_PRINTER_PRINT_SLEEP_PPU				3000
#define IEEE_PRINTER_PRINT_SLEEP_EXTRA			10000
#define IEEE_PRINTER_PRINT_SLEEP_F				2000

#define I3E_PRINT_PRINTERERROR					1

#define I3E_CHECK_NORMAL						1
#define I3E_CHECK_PD4							2

DWORD WINAPI PrinterThread(LPVOID lpParameter);
void start_print(__int64 &iTransaction, CONFIG *c, DEALER_CONFIG *dc, bool duplicate = false);
bool print_visit(void);

class CPrinter
{
	HANDLE hPort;
public:
	string stPrinterModel;
	DWORD dwPrinterModel;
	string stPrinterROM;
	bool PrintCheck(PAYMENT *p,CONFIG *c,DEALER_CONFIG *dc, vector<pair<string, string>> vPost, bool duplicate = false);
	bool PrintCheckThroughXSL(const __in wchar_t *lpszCheckCompleteContent, bool duplicate = false);
	DWORD PrintGIBDDCheck(PAYMENT *p,CONFIG *c, DEALER_CONFIG *dc);
	virtual bool PrintCashout(CASHOUT *co, CONFIG *c);
	virtual bool PrintTest(CONFIG *c);
	virtual bool PrintVisit(CONFIG *c, VISIT *v);
	//void PrintLine(char *szLine);
	virtual void PrintLine(bool crlf,char *szLine,...);
	virtual void PrintLine(bool crlf,wchar_t *szLine,...);
	virtual void PrintLine(bool crlf,string &stLine);
	virtual void PrintLine(bool crlf,BYTE b);
	void _printgibddpd4kv(PAYMENT *p, CONFIG *c,DEALER_CONFIG *dc,vector<m_kv>::iterator i);
	void _postprintspace(int inc = 0);
	virtual void Cut(bool full = true);
	void StatusInString(string &s,DWORD dwStatus);
	DWORD UpdateStatusOutside();
	virtual DWORD UpdateStatus(int nSleep = 0);
	virtual DWORD GetStatus();
	virtual bool Init(void);
	CPrinter();
	~CPrinter();

	HANDLE hThread;
	unsigned __int64 nThreadCheckPoint;
	wstring wsCom;
	CAtol *cAtol;
	HANDLE hMtxPrinter;

#ifdef _COMM_INCLUDE
	string get_comm(void);
	string get_model(void);
#endif

private:
	DWORD dwPrinterStatus;
	bool _getextra(PAYMENT *p,string &stDest,char *szName,...);
	void _printgibddkv(PAYMENT *p, CONFIG *c,DEALER_CONFIG *dc,vector<m_kv>::iterator i);
	void _printgibddch(PAYMENT *p, CONFIG *c);
	void _printline(bool crlf,string &st);
	void _clearstatus();
	void _appendstatus(DWORD dwStatus);
	void _prepare(DWORD dwCheckType);

	void _set_printmode(BYTE n);
	void _set_font(BYTE n);
	void _set_smooth(BYTE n);
	void _set_charset(BYTE n);
	void _set_justify(BYTE n);
	void _set_emphasis(BYTE n);
	void _set_underline(BYTE n);
	void _set_linefeed(BYTE n);
	void _set_spacing(BYTE n);
	void _set_rotate(BYTE n);
	void _set_leftmargin(BYTE n, BYTE l);
	void _set_cpi(BYTE n);
	void _put_init();
	void _put_eject(BYTE n);
	void _put_custom_secret();
	void _print_barcode(BYTE *bBarCode, int nLen);
	void _print_barcode2D(string sText);
	void _print_barcode2D(wstring wsText);
	void _set_barcode_height(BYTE bHeight = 80);
	void _set_barcode_width(BYTE bWidth = 2);
	void _sleep();
	DWORD _update_status();
protected:
	virtual void _update_spooler(void);
	bool bWinspool;
};

class CPrinterBase
{
	DWORD dwPrinterModel;
public:
	DWORD PrintGIBDDCheck(PAYMENT *p,CONFIG *c, DEALER_CONFIG *dc);
	bool PrintCashout(CASHOUT *co, CONFIG *c);
	virtual void PreparePrint(DWORD dwCheckType);
	virtual void PrintLine(DWORD dwFlags,bool crlf,char *szLine,...);
	virtual void EndPrint();
private:
	virtual void _printline(bool crlf,string &st);

	virtual void _set_printmode(BYTE n);
	virtual void _set_font(BYTE n);
	virtual void _set_smooth(BYTE n);
	virtual void _set_charset(BYTE n);
	virtual void _set_justify(BYTE n);
	virtual void _set_emphasis(BYTE n);
	virtual void _set_underline(BYTE n);
	virtual void _set_linefeed(BYTE n);
	virtual void _set_spacing(BYTE n);
	virtual void _set_rotate(BYTE n);
	virtual void _set_leftmargin(BYTE n, BYTE l);
	virtual void _put_init();
};

class CPrinter_sp500 : public CPrinterBase
{
	DWORD dwPrinterModel;
public:
private:
};
