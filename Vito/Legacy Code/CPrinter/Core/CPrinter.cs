﻿#region Usings

using System;
using System.Threading;

#endregion

namespace Core {
    public class CPrinter {
        private CAtol cAtol;
        private int dwPrinterStatus;
        private int dwPrinterModel;
        private App _app;
        private const long I3E_CHECK_NORMAL=0;
        private const long I3E_STATUS_OK=0;
        private const long I3E_DEVSTATUS_PRINTER=0;
        private const long I3E_STATUS_NOANSWER=0;
        private const long I3E_STATUS_OFFLINE=0;
        private const long IEEE_PRINTER_M_CITIZENPPU700=0;
        private const long IEEE_PRINTER_M_CITIZENS2000=0;

        private int hPort;
        private string stPrinterModel;
        private int CCNET_BILL_TYPES;
        private const long IEEE_PRINTER_M_STAR=0;
        private const long IEEE_PRINTER_M_CUSTOMTG2480=0;
        private const long IEEE_PRINTER_M_AV268=0;
        private const long IEEE_PRINTER_M_CUSTOMVKP80=0;

        public bool PrintVisit(CONFIG c, VISIT v) {
            var fret = false;
            if (UpdateStatus() == I3E_STATUS_OK) {
                //if(bWinspool)
                _update_spooler();
                _prepare(I3E_CHECK_NORMAL);

                _set_justify(1);
                this._set_emphasis(1);
                this.PrintLine(true, "Визит техника (инкассатора)");
                this._set_emphasis(0);
                _set_justify(0);
                this.PrintLine(true, "");

                this.PrintLine(true, "************Визит************");
                this.PrintLine(true, "ID терминала: %s", c.terminalId);
                this.PrintLine(true, "Номер терминала: %d", c.osmpId);
                /*this._set_emphasis(1);
		this.PrintLine(true,L"%d", c.osmpId);
		this._set_emphasis(0);*/

                this.PrintLine(true, "Дата визита: %.2d.%.2d.%d %.2d:%.2d:%.2d", v.datetimeS.Day.ToString(), v.datetimeS.Month.ToString(), v.datetimeS.Year.ToString(), v.datetimeS.Hour.ToString(), v.datetimeS.Minute.ToString(), v.datetimeS.Second.ToString());
                /*this._set_emphasis(1);
		PrintLine(true,"%.2d.%.2d.%d %.2d:%.2d:%.2d",v.datetimeS.wDay,v.datetimeS.wMonth,v.datetimeS.wYear,v.datetimeS.wHour,v.datetimeS.wMinute,v.datetimeS.wSecond);
		this._set_emphasis(0);*/
                 
                this.PrintLine(true, "Номер визита: %I64d", v.id);
                /*this._set_emphasis(1);
		this.PrintLine(true,L"%I64d", v.id);
		this._set_emphasis(0);*/

                this.PrintLine(true, "Номер техника: %d", v.i.uid);
                /*this._set_emphasis(1);
		this.PrintLine(true,L"%d", v.i.uid);
		this._set_emphasis(0);*/

                this.PrintLine(true, "Имя техника: %s", v.i.sName);
                /*this._set_emphasis(1);
		this.PrintLine(true, "%s", v.i.sName.c_str());
		this._set_emphasis(0);*/

                this.PrintLine(true, "");

                this.PrintLine(true, "************Оборудование************");
                this.PrintLine(true, "Принтер:");
                this.PrintLine(true, "    Модель: %s", this.stPrinterModel);
                /*this._set_emphasis(1);
		this.PrintLine(true, "%s", this.stPrinterModel.c_str());
		this._set_emphasis(0);*/

                this.PrintLine (true, "    Состояние: %s", ((dwPrinterStatus != 0) ? "Неисправен" : "ОК"));
                /*this._set_emphasis(1);
		if (this.dwPrinterStatus == 0) {
			this.PrintLine(true, "ОК");
		} else {
			this.PrintLine(true, "Неисправен!");
		}
		this._set_emphasis(0);*/

                PRINTER_CONFIG pc;
                _app.m_pCrypto.getPrinterConfig(out pc);

                this.PrintLine(true, "    Остаток ленты: %.2f м.", ((float) pc.rollSize/1000).ToString());
                /*this._set_emphasis(1);
		this.PrintLine(true, "%.2f м.", (float)pc.rollSize / 1000);
		this._set_emphasis(0);*/

                if (cAtol.dwPrinterModel != 0) {
                    if (c.bFisk) {
                        cAtol.PrintLine("    Фискальный режим: Да");
                        if (cAtol.cbStatusEx.bZFreeSupported) {
                            cAtol.PrintLine("    Z-отчеты: Ост. %d", cAtol.cbStatusEx.bZFree.ToString());
                        }
                        else {
                            cAtol.PrintLine("    Z-отчеты: Не поддерж.");
                        }
                    }
                    else {
                        //if fisk
                        cAtol.PrintLine("    Фискальный режим: Нет");
                    } //if fisk
                } //if atol

                this.PrintLine(true, "Купюроприемник:");
                this.PrintLine(true, "    Модель: %s", _app.m_pCashCode.stModelName);
                /*this._set_emphasis(1);
		this.PrintLine(true, "%s", _app.m_pCashCode.stModelName.c_str());
		this._set_emphasis(0);*/

                this.PrintLine(true, "    Состояние: %s", ((dwPrinterStatus != 0) ? "Неисправен" : "ОК"));
                /*this._set_emphasis(1);
		if (_app.m_pCashCode.dwLastState == 0) {
			this.PrintLine(true, "ОК");
		} else {
			this.PrintLine(true, "Неисправен!");
		}
		this._set_emphasis(0);*/

                var nBills = new int[CCNET_BILL_TYPES];
                _app.m_pCrypto.getCcnetStat(out nBills);
                this.PrintLine(true, "    Кол-во купюр: %d", getBillCount(nBills).ToString());
                /*this._set_emphasis(1);
		this.PrintLine(true, "%d", getBillCount(nBills));
		this._set_emphasis(0);*/

                this.PrintLine(true, "Сторожевой таймер: %s", ((_app.m_pWatchDog.WatchDogVersion) ? _app.m_pWatchDog.WatchDogVersionName : "Отсутствует"));
                /*if (_app.m_pWatchDog.WatchDogVersion != 0) {
			this.PrintLine(false,"    Модель: ");
			this._set_emphasis(1);
			this.PrintLine(true, "%s", _app.m_pWatchDog.WatchDogVersionName.c_str());
			this._set_emphasis(0);
		} else {
			this.PrintLine(true,"    Отсутствует");
		}*/

                this.PrintLine(true, "");

                this.PrintLine(true, "************Связь************");
                this.PrintLine(true, "Платежей в очереди: %d", _app.m_pCrypto.getPaymentsCount().ToString());
                /*this._set_emphasis(1);
		this.PrintLine(true, "%d", _app.m_pCrypto.getPaymentsCount());
		this._set_emphasis(0);*/


                this._postprintspace(10);
                this.Cut();
                _app.m_pCrypto.decPrinterConfig(5);
                fret = true;
            }
            return fret;
        }

        private void Cut() {
            
        }

        private void _postprintspace(int i) {
            
        }

        private int getBillCount(int[] nBills) {
            return 0;
        }

        private void PrintLine(bool bcrlf, params string[] szLine) {
            
        }

        private void _set_emphasis(byte @byte) {
	        if (this.dwPrinterModel == IEEE_PRINTER_M_CITIZENPPU700 || 
                this.dwPrinterModel == IEEE_PRINTER_M_CITIZENS2000 || 
                this.dwPrinterModel == IEEE_PRINTER_M_CUSTOMVKP80 || this.dwPrinterModel == IEEE_PRINTER_M_AV268 || this.dwPrinterModel == IEEE_PRINTER_M_CUSTOMTG2480) {
		        var b = new byte[3] {0x1B,0x45,@byte};
		        WriteComPort(hPort,b,b.Length,0,0,0,0);
	        } else if (this.dwPrinterModel == IEEE_PRINTER_M_STAR) {
		        var b = new byte[2] {0x1B,0x45};
		        b[1] += @byte;
		        WriteComPort(hPort,b,b.Length,0,0,0,0);
	        }
        }


        private void _set_justify(byte @byte) {
            var b = new byte[3] {0x1B, 0x61, @byte};
            WriteComPort(hPort, b, 3, 0, 0, 0, 0);
        }

        private int WriteComPort(object hPort, byte[] bytes, int i, int i1, int i2, int i3, int i4) {
            return 0;
        }

        private void _prepare(long dwCheckType) {
            throw new NotImplementedException();
        }

        private void _update_spooler() {
            throw new NotImplementedException();
        }

        private long UpdateStatus(int nSleep = 0) {
            var dwRet = 0;

            if (cAtol.dwPrinterModel != 0) {
                for (var i = 1; i <= 5; i++) {
                    dwRet = cAtol.UpdateStatus();
                    if ((dwRet & I3E_STATUS_OFFLINE) != 0) {
                        Thread.Sleep(1000);
                    }
                    else {
                        //if offline
                        break;
                    }
                } //for
                dwPrinterStatus = dwRet;
            }
            else {
                for (var i = 1; i <= 5; i++) {
                    dwRet = _update_status();
                    if ((dwRet & I3E_STATUS_NOANSWER) != 0 || (dwRet & I3E_STATUS_OFFLINE) != 0) {
                        Thread.Sleep(1000);
                    }
                    else {
                        //if offline
                        break;
                    }
                } //for
            }

            _app.SetDeviceStatus(I3E_DEVSTATUS_PRINTER, dwPrinterStatus);
            return dwRet;
        }

        private int _update_status() {
            throw new NotImplementedException();
        }
    }
}