#pragma once
#include "gtest\gtest.h"
#include "Father.h"

#define ASSERT_CONTAINS(value, subString) ASSERT_TRUE(value.find(subString) != string::npos)

class BaseTest : public ::testing::Test {
public:
    BaseTest::BaseTest() : Create() {};
    Father Create;
};

