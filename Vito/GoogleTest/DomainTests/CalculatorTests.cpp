#include "gtest\gtest.h"
#include "Calculator.h"
#include "BaseTest.h"

class CalculatorTests : public BaseTest {};

TEST_F(CalculatorTests, CanAdd) {
    Calculator calculator = Create.NewCalculator();

    int sum = calculator.Add(1, 1);
    
    ASSERT_EQ(2, sum);
}